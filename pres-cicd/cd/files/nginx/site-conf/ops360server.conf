log_format proxy_log '[$time_local] $remote_addr - $remote_user - $server_name to: $upstream_addr: uri: $uri: $request upstream_response_time $upstream_response_time msec $msec request_time $request_time';

server {
    listen 443 ssl;
    server_name {{ host }};
    
    root /var/www/;
    index index.html;
    
    ssl_certificate      /etc/ssl/certs/ops360server.crt;
    ssl_certificate_key  /etc/ssl/private/ops360server.key;
  
    access_log /var/log/nginx/access.log upstream_logging;
    
    error_log /var/log/nginx/error.log debug; 

    add_header X-Request-ID $request_id; # Return to client  

    underscores_in_headers on;
   
    rewrite_log on;
 
    ssl_prefer_server_ciphers on;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2; # not possible to do exclusive
    ssl_ciphers 'EDH+CAMELLIA:EDH+aRSA:EECDH+aRSA+AESGCM:EECDH+aRSA+SHA384:EECDH+aRSA+SHA256:EECDH:+CAMELLIA256:+AES256:+CAMELLIA128:+AES128:+SSLv3:!aNULL:!eNULL:!LOW:!3DES:!MD5:!EXP:!PSK:!DSS:!RC4:!SEED:!ECDSA:CAMELLIA256-SHA:AES256-SHA:CAMELLIA128-SHA:AES128-SHA';
    add_header Strict-Transport-Security max-age=15768000; # six months
  
    keepalive_timeout    70;
   
    proxy_http_version 1.1;
 
    location / {
        proxy_pass http://core-ui:80;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-Proto https;
        proxy_redirect off;

        access_log  /var/log/nginx/access.log  upstream_logging;
    }

    location ~* \*\config.json {
        add_header Cache-Control 'max-age=604800,public,immutable';

	proxy_pass http://core-ui:80;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-Proto https;
        proxy_redirect off;

        access_log  /var/log/nginx/access.log  upstream_logging;
    }

    location ~* \*\navigation.json {
        add_header Cache-Control 'max-age=604800,public,immutable';

        proxy_pass http://core-ui:80;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-Proto https;
        proxy_redirect off;

        access_log  /var/log/nginx/access.log  upstream_logging;
    }

    location /data-browsing/ {
        rewrite ^/data-browsing(/.*) /browse/$1 last;
        access_log  /var/log/nginx/access.log  upstream_logging;
	
	add_header Cache-Control no-cache;
        expires 1s;
    }

    location /browse/static/ {
        # default docker DNS resolver set as resolver here so it can resolve location if you recreate docker container
        resolver 127.0.0.11 valid=30s;

        rewrite ^/browse/static/(.*)$ /static/$1 break;

        proxy_pass http://browsing-ui:80;
        proxy_pass_request_headers      on;

        # browser cache
        add_header Last-Modified $date_gmt;
        add_header Cache-Control 'max-age=604800,public,immutable';

        # kill nginx cache
        proxy_no_cache 1;
        # even if cached, don't try to use it
        proxy_cache_bypass 1;

        access_log  /var/log/nginx/access.log  upstream_logging;
    }

    location /browse/ {
        # default docker DNS resolver set as resolver here so it can resolve location if you recreate docker container
        resolver 127.0.0.11 valid=30s;

        rewrite ^/browse(/.*)$ /$1 break;

        proxy_pass http://browsing-ui:80;

        access_log  /var/log/nginx/access.log  upstream_logging;

        proxy_pass_request_headers      on;

        # kill browser cache
        add_header Last-Modified $date_gmt;
        add_header Cache-Control 'max-age=600';
        if_modified_since off;
        expires off;
        etag off;

        # kill nginx cache
        proxy_no_cache 1;
        # even if cached, don't try to use it
        proxy_cache_bypass 1;
    }

   location /ac/ {
        # default docker DNS resolver set as resolver here so it can resolve location if you recreate docker container
        resolver 127.0.0.11 valid=30s;

        rewrite  ^  $request_uri;       # get original URI
        rewrite  ^/ac(/.*) $1 break;  # drop /ac
        return 400;                     # if the second rewrite won't match

        proxy_pass http://bdms-service:9000$uri;

        # browser cache
        #add_header Last-Modified $date_gmt;
        add_header Cache-Control 'no-store';
        #add_header Cache-Control 'max-age=604800,public,immutable';
        proxy_hide_header Vary;
 
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-Proto https;
        proxy_pass_request_headers      on;

        access_log  /var/log/nginx/access.log  upstream_logging;
    }

   location /auth/ {
        proxy_pass http://auth-service:9000/auth/;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-Proto https;
        proxy_redirect off;
        proxy_http_version 1.1;

        access_log  /var/log/nginx/access.log  upstream_logging;

    }

   location /ac-site/ {
        proxy_pass http://www.asset-control.com/;

        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-Proto https;
        proxy_pass_request_headers      on;

        access_log  /var/log/nginx/access.log  upstream_logging;
    }

    location /issue-service/ {
        proxy_pass http://issue-service:9000/issue-service/;

        access_log  /var/log/nginx/access.log  upstream_logging;
    }

    location /cleansing/ {
        proxy_pass http://data-cleansing-ui:80/;

        access_log  /var/log/nginx/access.log  upstream_logging;
    }

     location /select/ {
        proxy_pass http://ac-select:9000/select/;

        access_log  /var/log/nginx/access.log  upstream_logging;
     }

    location /data-model-admin/ {
        #rewrite ^/data-model-admin(/.*)$ $1 break;

        proxy_pass http://data-model-admin-ui/;

        access_log  /var/log/nginx/access.log  upstream_logging;
    }

    location /data-view-admin/ {
        #rewrite ^/data-model-admin(/.*)$ $1 break;
        proxy_pass http://data-view-admin-ui/;

        access_log  /var/log/nginx/access.log  upstream_logging;
    }

    location /bpm-admin/ {
        #rewrite ^/data-model-admin(/.*)$ $1 break;
        proxy_pass http://bpm-admin-ui/;

        access_log  /var/log/nginx/access.log  upstream_logging;
    }

   location /lineage/ {
        proxy_pass http://ac-dl-read:9000/;

        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-Proto https;
        proxy_pass_request_headers      on;

        access_log  /var/log/nginx/access.log  upstream_logging;
    }

    location /task-service/ {
        #rewrite ^/task-servicex(/.*)$ $1 break;

        proxy_pass http://task-service-ui/;

        access_log  /var/log/nginx/access.log  upstream_logging;
    }

    location /tasks/ {
        proxy_pass http://task-service:9000/;

        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-Proto https;
        proxy_pass_request_headers      on;

        access_log  /var/log/nginx/access.log  upstream_logging;
    }

    location /datasets/ {

        proxy_pass http://data-sets-service:9000/;

        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-Proto https;
        proxy_pass_request_headers      on;

        access_log  /var/log/nginx/access.log  upstream_logging;
    }

    location /script-executor/ {

        proxy_pass http://script-executor:8080/;

        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-Proto https;
        proxy_pass_request_headers      on;

        access_log  /var/log/nginx/access.log  upstream_logging;
    }

    location /lineage-statistics/ {
        #rewrite ^/lineage-statistics(/.*)$ $1 break;

        proxy_pass http://lineage-stats-ui/;

        access_log  /var/log/nginx/access.log  upstream_logging;
    }

    location /lineage-stats/ {
        proxy_pass http://lineage-stats:9000/;

        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-Proto https;
        proxy_pass_request_headers      on;

        access_log  /var/log/nginx/access.log  upstream_logging;
    }
}
