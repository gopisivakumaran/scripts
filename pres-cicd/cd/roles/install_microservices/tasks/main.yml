- name: Register hostname of target
  shell: "uname -n"
  register: host
  tags:
    - host

- set_fact:
    host={{ host.stdout }}
  tags:
    - host

- name: Create docker directory
  file:
    path: /home/acdba/docker
    state: directory

- name: Create docker bdms directory
  file:
    path: /home/acdba/docker/bdms-service
    state: directory

- name: Create docker issue-service directory
  file:
    path: /home/acdba/docker/issue-service
    state: directory

- name: Install docker-compose
  shell: |
    sudo curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/bin/docker-compose
    sudo chmod +x /usr/bin/docker-compose

- name: Add user to docker group
  shell: sudo usermod -aG docker acdba
  tags:
    - docker

- name: Copy docker compose file to target
  template:
    src: "{{ playbook_dir }}/files/docker-compose.yml"
    dest: "/home/acdba/docker/docker-compose.yml"
  tags:
    - copycompose

- name: Create docker network ac-net
  shell: docker network create ac-net || true
  tags:
    - docker

- name: Start cassandra microservice
  shell: |
    docker volume create --name=cassandra_data
    cd /home/acdba/docker; docker-compose up -d cassandra
  tags:
    - cassandra

- name: Create nginx directory
  file:
    path: /home/acdba/docker/nginx
    state: directory
  tags:
    - nginx

- name: Create nginx/site-conf directory
  file:
    path: /home/acdba/docker/nginx/site-conf
    state: directory
  tags:
    - nginx

- name: Copy nginx/conf directory
  copy:
    src: "{{ playbook_dir }}/files/nginx/conf"
    dest: "/home/acdba/docker/nginx"
  tags:
    - nginx

- name: Copy nginx/ssl directory
  copy:
    src: "{{ playbook_dir }}/files/nginx/ssl"
    dest: "/home/acdba/docker/nginx"
  tags:
    - nginx

- name: Copy nginx template
  template:
    src: "{{ playbook_dir }}/files/nginx/site-conf/ops360server.conf"
    dest: "/home/acdba/docker/nginx/site-conf/ops360server.conf"
  tags:
    - nginx-debug

- name: Start nginx microservice
  shell: cd /home/acdba/docker; docker-compose up -d nginx
  tags:
    - nginx

- name: Copy daemon.json
  copy:
    src: "{{ playbook_dir }}/files/daemon.json"
    dest: "/etc/docker"
    owner: root
    group: root
  become: true
  tags:
    daemon

- name: Restart dockerd service
  shell: sudo systemctl restart docker
  tags:
    daemon

- name: Restart cassandra
  shell: docker restart cassandra 

- name: Copy core-ui/nginx/conf directory
  copy:
    src: "{{ playbook_dir }}/files/core-ui"
    dest: "/home/acdba/docker"
  tags:
    - core-ui

- name: Start core-ui microservice
  shell: cd /home/acdba/docker; docker-compose up -d core-ui
  tags:
    - core-ui

- name: Copy auth directory
  copy:
    src: "{{ playbook_dir }}/files/auth"
    dest: "/home/acdba/docker"
  tags:
    - auth

- name: Create auth DB directory
  file:
    path: /home/acdba/docker/auth/DB
    state: directory
    owner: "1002"
    group: "root"
  become: true
  tags:
    - auth

- name: Start auth-service 
  shell: cd /home/acdba/docker; docker-compose up -d auth-service
  tags:
    - auth

- name: Copy bdms directory
  copy:
    src: "{{ playbook_dir }}/files/bdms"
    dest: "/home/acdba/docker"
  tags:
    - bdms

- name: Get private ip
  shell: "/usr/sbin/ip route get 1.2.3.4 | awk '{print $7}'"
  register: ip
  #ignore_errors: true
  tags:
    - ip

- set_fact:
    ip={{ ip.stdout }}
  tags:
    - ip

- name: Copy bdms docker template
  template:
    src: "{{ playbook_dir }}/files/docker-compose_bdms.yml"
    dest: "/home/acdba/docker/bdms-service"
  tags:
    - bdms-test

- name: Create bdms keyspace
  shell: >
    docker exec -i cassandra cqlsh -e "create keyspace bdms with replication = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 }"
  retries: 3
  delay: 1
  register: result
  until: result.rc == 0
  tags:
    - debug

- name: Start bdms-service
  shell: cd ~/docker/bdms-service; docker-compose -f docker-compose_bdms.yml up -d
  tags:
    - bdms

- name: Start browsing-ui service
  shell: cd ~/docker; docker-compose up -d browsing-ui
  tags:
    - browsing-ui

- name: Start admin ui services
  shell: cd ~/docker; docker-compose up -d data-model-admin-ui data-view-admin-ui bpm-admin-ui
  tags:
    - admin-ui

- name: Start workflow service
  shell: . /home/acdba/ac/bin/acenv.sh && nohup /home/acdba/ac/workflowengine/bin/start.sh >/dev/null 2>&1 &
  become: true
  become_user: acdba
  tags:
    - wfe

- name: Copy issue-service docker template
  template:
    src: "{{ playbook_dir }}/files/docker-compose_issue.yml"
    dest: "/home/acdba/docker/issue-service"
  tags:
    - issue-service

- name: Copy ops-server directory
  copy:
    src: "{{ playbook_dir }}/files/ops-server"
    dest: "/home/acdba/docker"
  tags:
    - ops-server

- name: Copy issue-service directory
  copy:
    src: "{{ playbook_dir }}/files/issue-service"
    dest: "/home/acdba/docker"
  tags:
    - issue-service

- name: Change permissions of issue-service db directory
  file:
    path: /home/acdba/docker/issue-service/db
    mode: '0777'
  tags:
    - issue-service

- name: Start issue-service
  shell: cd ~/docker/issue-service; docker-compose -f docker-compose_issue.yml up -d
  tags:
    - issue-service

- name: Start data-cleansing-ui
  shell: cd ~/docker; docker-compose up -d data-cleansing-ui
  tags:
    - dcui

- name: Copy ac-select directory
  copy:
    src: "{{ playbook_dir }}/files/ac-select"
    dest: "/home/acdba/docker"
  tags:
    - ac-select

- name: Change owner of ac-select database
  file:
    path: /home/acdba/docker/ac-select/database
    owner: root
    group: root
    mode: 0775
  become: true
  tags:
    - ac-select

- name: Start ac-select service
  shell: cd ~/docker; docker-compose up -d ac-select
  tags:
    - ac-select

- name: Create lineage directory
  file:
    path: /home/acdba/docker/lineage
    state: directory
  tags:
    - lineage

- name: Create lineage/lineage_fails directory
  file:
    path: /home/acdba/docker/lineage/lineage_fails
    state: directory
  tags:
    - lineage

- name: Create lineage/lineage directory
  file:
    path: /home/acdba/docker/lineage/lineage
    owner: "9001"
    group: acdba
    state: directory
  become: true
  tags:
    - lineage

- name: Create lineage/lineage/queue directory
  file:
    path: /home/acdba/docker/lineage/lineage/queue
    owner: acdba
    group: acdba
    state: directory
  become: true
  tags:
    - lineage

- name: Copy logger.xml file
  copy:
    src: "{{ playbook_dir }}/files/lineage/logger.xml"
    dest: "/home/acdba/docker/lineage"
  tags:
    - lineage

- name: Copy logger.xml file
  copy:
    src: "{{ playbook_dir }}/files/lineage/services-logger.xml"
    dest: "/home/acdba/docker/lineage"
  tags:
    - lineage

- name: Copy lineage.cql file
  copy:
    src: "{{ playbook_dir }}/files/lineage/lineage.cql"
    dest: "/home/acdba/docker/lineage"
  tags:
    - lineage

- name: Create lineage keyspace
  shell: >
    docker exec -i cassandra cqlsh -e "create keyspace lineage with replication = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 }"
  tags:
    - lineage

- name: Load cql schema for lineage
  shell: docker cp ~/docker/lineage/lineage.cql cassandra:/home ; docker exec -i cassandra cqlsh -f /home/lineage.cql
  tags:
    - lineage  

- name: Start ac-dl services
  shell: cd ~/docker; docker-compose up -d ac-dl-read ac-dl-ingest ac-dl-propagation
  tags:
    - lineage

- name: Create data propagation folder 1
  file:
    path: /home/acdba/docker/deduplication-service/propagation/files/monitor
    mode: 0776
    state: directory
  tags:
    - data-propagation-test

- name: Create data propagation folder 2
  file:
    path: /home/acdba/docker/deduplication-service/propagation/files/success
    mode: 0776
    state: directory
  tags:
    - data-propagation-test

- name: Create data propagation folder 3
  file:
    path: /home/acdba/docker/deduplication-service/propagation/files/error
    mode: 0776
    state: directory
  tags:
    - data-propagation-test

- name: Start data propagation services
  shell: cd ~/docker; docker-compose up -d dp-propagation-service dp-ingestion-service dp-read-service
  tags:
    - data-propagation

- name: Restart ac-issue-service
  shell: docker restart ac-issue-service

- name: Copy tasks-service folder
  copy:
    src: "{{ playbook_dir }}/files/task-service"
    dest: "/home/acdba/docker"
  tags:
    - task-service

- name: Change permissions of task-service files
  shell: chown -R root:root /home/acdba/docker/task-service
  become: true

- name: Create tasks keyspace
  shell: >
    docker exec -i cassandra cqlsh -e "create keyspace tasks with replication = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 }"
  tags:
    - task-service 

- name: Create tasks schema
  shell: docker cp ~/docker/task-service/tasks.cql cassandra:/home ; docker exec -i cassandra cqlsh -f /home/tasks.cql
  tags:
    - task-service

- name: Start task-service
  shell: cd  ~/docker; docker-compose up -d task-service
  tags:
    - task-service

- name: Start task-service-ui
  shell: cd  ~/docker; docker-compose up -d task-service-ui
  tags:
    - task-service

- name: Copy script-executor directory
  copy:
    src: "{{ playbook_dir }}/files/script-executor-service"
    dest: "/home/acdba/docker"
  tags:
    - ses

- name: Create script executor docker directory
  file:
    path: /home/acdba/docker/script-executor
    state: directory
  tags:
    - ses

- name: Copy script-executor docker-compose
  template:
    src: "{{ playbook_dir }}/files/docker-compose_script-executor.yml"
    dest: "/home/acdba/docker/script-executor"
  tags:
    - ses

- name: Start script execution service
  shell: cd ~/docker/script-executor; docker-compose -f docker-compose_script-executor.yml up -d
  tags:
    - ses

- name: Create datasets keyspace
  shell: >
    docker exec -i cassandra cqlsh -e "create keyspace datasets with replication = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 }"
  tags:
    - dss

- name: Copy datasets directory
  copy:
    src: "{{ playbook_dir }}/files/datasets"
    dest: "/home/acdba/docker"
  tags:
    - dss

- name: Load cql schema for datasets
  shell: docker cp ~/docker/datasets/datasets.cql cassandra:/home ; docker exec -i cassandra cqlsh -f /home/datasets.cql
  tags:
    - dss

- name: Start data sets service
  shell: cd ~/docker; docker-compose up -d data-sets-service
  tags:
    - dss

- name: Copy lineage logback.xml
  copy:
    src: "{{ playbook_dir }}/files/lineage/logback.xml"
    dest: "/home/acdba/docker/lineage"
  tags:
    - lineage-stats

- name: Copy lineage stats directory
  copy:
    src: "{{ playbook_dir }}/files/lineage/stats"
    dest: "/home/acdba/docker/lineage"
  tags:
    - lineage-stats

- name: Create lineage_stats keyspace
  shell: >
    docker exec -i cassandra cqlsh -e "create keyspace if not exists lineage_stats with replication = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 }"
  tags:
    - lineage-stats

- name: Copy lineage_stats.cql file
  copy:
    src: "{{ playbook_dir }}/files/lineage/lineage_stats.cql"
    dest: "/home/acdba/docker/lineage"
  tags:
    - lineage-stats

- name: Load cql schema for lineage_stats
  shell: docker cp ~/docker/lineage/lineage_stats.cql cassandra:/home ; docker exec -i cassandra cqlsh -f /home/lineage_stats.cql
  tags:
    - lineage-stats

- name: Start lineage stats service
  shell: cd ~/docker; docker-compose up -d lineage-stats
  tags:
    - lineage-stats

- name: Start lineage stats ui
  shell: cd ~/docker; docker-compose up -d lineage-stats-ui
  tags:
    - lineage-stats

- name: Restart nginx
  shell: docker restart nginx

- name: Restart ac-issue-service
  shell: docker restart ac-issue-service

- name: Copy authorization script
  template:
    src: "{{ playbook_dir }}/files/authorization.sh"
    dest: "/home/acdba/docker/auth/authorization.sh"
    mode: 0776

- name: Give service permissions to admin user
  shell: /home/acdba/docker/auth/authorization.sh

- name: Bdm-tool tar name
  shell: echo {{ business_domain_model_generator }} | awk -F/ '{print $NF}'
  register: bdm_package
  tags:
    - bdm-tool

- set_fact:
    bdm_package={{ bdm_package.stdout }}
  tags:
    - bdm-tool

- name: Download and install bdm-tool from nexus
  shell: |
    cd /home/acdba; wget {{ business_domain_model_generator }} 
    tar zxf {{ bdm_package }} && rm {{ bdm_package }}
  tags:
    - bdm-tool 
