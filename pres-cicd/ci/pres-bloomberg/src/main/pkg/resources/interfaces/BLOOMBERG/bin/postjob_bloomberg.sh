#!/bin/ksh

. ac_utilities.sh

JOBDEF=$1
STATUS=$2

if test -s "$JOBDEF"
then
 ac_log_info "Job definition in >$JOBDEF<"
else
 ac_log_fatal "Invalid or non existing job definition >$JOBDEF<"
 exit 1
fi

if test "$STATUS" != "SUCCESS"
then
 ac_log_fatal "Interface run was not successful. Not running post job."
 exit 1
fi

JOBLOG=$JOBDEF.log
IFCDIR=$(dirname $JOBLOG)
TGTDIR=$AC_DATA/vendor_files/bloomberg/normalization
TGTRID=$(basename $IFCDIR)
TGTLOC=$TGTDIR/$TGTRID.ado
mkdir -p $TGTDIR
check_rc "Could not create >$TGTDIR<"

echo "acAdoId" > $TGTLOC
cat $JOBLOG | perl -ne 'if (/Basic Ado (?:touched|created).*"\[?([A-Z][A-Z0-9]+\.[^\[\]"]+)/ && !$A{$1}++){print $1."\n"}' >> $TGTLOC
check_rc "Error determining base ADOs"
ac_log_info "Base ADOs in >$TGTLOC<"

exit 0
