#!/bin/ksh

cd $(dirname $0)/..
. ${AC_PROJECT:-$HOME/pkg}/bin/install_functions.sh

install_interface -c -i BLOOMBERG \
  -t interfaces/BLOOMBERG/config/BLOOMBERG.IF_EQ.properties \
  -t interfaces/BLOOMBERG/config/BLOOMBERG.Create_N0.properties \
  -t local/files/BLOOMBERG.cnf

