#!/usr/bin/ksh
#
# jobprovider script for Interface Engine
#
# This jobprovider simply makes a job for the file provided in the variable INPUTFILE
#
#------------------------------------------------------------------------------
set -eu
prgname=${0##*/}
prgdir=$(dirname "$0")
alias local='typeset'

function fatal {
    echo "FATAL $prgname - ${1:-}" >&2
    exit 1
}

function info {
    echo "INFO  $prgname - $*"
}

function warn {
    echo "WARN  $prgname - $*"
}

function error {
    echo "ERROR $prgname - $*"
}

# generateJob <datafile>
function generateJob {
    local datafile="$1"
        local queue=""
        local jobdef=""
    local fileno=0

        jobseq=$((jobseq + 1))
        jobdef="job-${jobseq}-${queue:-default}"
        {
                echo "# Generated $(date)"
                echo "# interface=$interface task=$task date=$date"
                echo "# jobprovider=$prgname"
                [[ -z "$queue" ]] || echo "job.queuename=$queue"

            echo "job.datafile.0.pathname=$datafile"

        } > "$jobdef" || error "Failed to create job definition: $jobdef"

        printf "job\t$jobdef\n"
}

#------------------------------------------------------------------------------
# parse args...
#
interface=
task=
date=
jobseq=0

while getopts ":t:i:d:" opt
do
    case "$opt" in
    t)  task=$OPTARG ;;
    i)  interface=$OPTARG ;;
    d)  date=$OPTARG ;;
    esac
done
shift $((OPTIND - 1))

#------------------------------------------------------------------------------
# main
info "interface=$interface task=$task date=$date ..."

errormsg="Variable: INPUTFILE has not been set but is required by this jobprovider."
inputfile=${INPUTFILE:?"$errormsg"}
[[ -s "$inputfile" ]] || fatal "Invalid input file: $inputfile"

generateJob "$inputfile"

[[ $jobseq > 0 ]] || warn "No jobs created! Missing feed file?"

exit 0

