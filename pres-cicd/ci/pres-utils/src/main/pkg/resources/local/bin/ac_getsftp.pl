#!/usr/bin/env perl
#
# Generic sftp (upload and) download script.
# Download all files that match the pattern(s) and have changed since the last download.
# Files are downloaded in the current directory.
# Files are NOT downloaded if the size is the same and the timestamp is not newer.
# Previous download information is stored in a .ftpcache.* file
# Unchanged files are not reported back as changed (using MD5 in .ftpcache.* file).
# A single request file can be uploaded and the script can wait for and download a report file.
#
# By default files older than 7 days are not downloaded (-m option)
# A request-report timeout (5:00) and output timeout (1:20:00) allows the script to wait for files.
# The -n option specifies the number of files to wait for
# $FTP_REQ_TIMEOUT and $FTP_OUT_TIMEOUT control the timeout (>= 0 sec).
# $FTP_CHANGE_TIMEOUT controls how long to wait after a change is detected
# $FTP_STAT_SLEEP and $FTP_LS_SLEEP control the sleep time between stat and ls calls
#
# Usage: ac_getsftp.pl -h host '*.txt'
#  -I initialize cache only, don't download
#  -u user
#  -p password
#  -k ssh-key-file
#  -d remote directory and optional req, out and rpt dir (comma-separated)
#  -m max-file-age in hours
#  -s skip download if the file size did not change and the timestamp is less then x seconds newer
#  -o number of times to retry opening the sftp connection
#  -r request-file
#  -R report-file
#  -n number of output files expected
#  -w hhmm|sec: wait until time hhmm or sec for starting downloads. If hh < 24 then wait today else tomorrow.
#  -e use regex for output file spec
#  -t strftime-spec: add timestamp to output file name using %Y%m%d etc
#
# Uses package Net::SFTP::Foreign ($AC_SYSTEM/local/lib/perl5/Net)
#

use strict;
BEGIN {
  my ($dir) = $0 =~ /^(.*\/)/;
  $dir = "./" if (!$dir);
  push @INC, "${dir}../lib/perl5", "${dir}lib", $dir;
}

use Getopt::Std;
use Cwd;
use POSIX qw(strftime);
use Net::SFTP::Foreign;
use Net::SFTP::Foreign::Constants qw(:error :status);
use Fcntl qw(:mode);
use Time::Local;
use Digest::MD5;

use ac_utilities;

my $sftp;
my ($host, $user, $passwd, $debug, $keypath, $port);
my ($dir, $filespec);
my ($req_timeout, $out_timeout) = (5*60, 80*60);   # (0:05:00, 1:20:00)
my ($change_timeout, $stat_sleep, $ls_sleep) = (15, 5, 38);
my $cachefile = '.ftpcache';
my %cache = ();
my %cachemd5 = ();
my $cachechanged = 0;
my %file = ();
my %filesize = ();
my %filetime = ();
my %opts;
my $init_only;
my $max_open_tries = 5;
my $curtime = time();
my $mintime = $curtime - 7 * 86400;       # Don't download anything older than 7 days
my $timediff = 30;
my $reqfile;
my $rptfile;
my $out_count = 0;
my $server_timediff;
my $listfile;
my $waitout;
my $outtmspec;

$host = $ENV{FTP_HOST} || '';
$user = $ENV{FTP_USER} || '';
$passwd = $ENV{FTP_PASSWD} || '';
$keypath = $ENV{FTP_KEY} || '';
$debug = $ENV{FTP_DEBUG};

$req_timeout = $ENV{FTP_REQ_TIMEOUT}+0 if ($ENV{FTP_REQ_TIMEOUT} =~ /^\d+$/);
$out_timeout = $ENV{FTP_OUT_TIMEOUT}+0 if ($ENV{FTP_OUT_TIMEOUT} =~ /^\d+$/);
$change_timeout = $ENV{FTP_CHANGE_TIMEOUT}+0 if ($ENV{FTP_CHANGE_TIMEOUT} =~ /^\d+$/);
$stat_sleep = $ENV{FTP_STAT_SLEEP}+0 if ($ENV{FTP_STAT_SLEEP} =~ /^\d+$/);
$ls_sleep = $ENV{FTP_LS_SLEEP}+0 if ($ENV{FTP_LS_SLEEP}+0 =~ /^\d+$/);

$stat_sleep = 2 unless ($stat_sleep > 2);   # prevent sending too many requests
$ls_sleep = 10 unless ($ls_sleep > 10);     # some data vendors have an explicit limit specified !

getopts('IR:cd:eh:k:l:m:n:o:p:r:s:t:u:w:', \%opts);

$init_only = 1 if (defined $opts{I});
$mintime = 0 if ($init_only);

$host = $opts{h} if (defined $opts{h});
$user = $opts{u} if (defined $opts{u});
$passwd = $opts{p} if (defined $opts{p});
$keypath = $opts{k} if (defined $opts{k});
$dir = $opts{d} if (defined $opts{d});
$max_open_tries = $opts{o} if (defined $opts{o});
if (defined $opts{m}) {
  $mintime = $curtime - $opts{m} * 3600 if ($opts{m} >= 0);
  $mintime = undef if ($opts{m} eq '-' || $opts{m} eq '-1');
}
$timediff = $opts{s} if (defined $opts{s});
$reqfile = $opts{r} if (defined $opts{r});
$rptfile = $opts{R} if (defined $opts{R});
$out_count = $opts{n} if (defined $opts{n} && !$init_only);
$listfile = $opts{l} if (defined $opts{l});
$waitout = $opts{w} if (defined $opts{w});
$outtmspec = $opts{t} if (defined $opts{t});

if ($host =~ /^(.*):(\d+)$/) {
  ($host, $port) = ($1, $2);
}

my @dir0 = split(/,/, $dir);
$dir = $dir0[0];
my $indir = $dir0[1];
my $outdir = $dir0[2];
my $rptdir = $dir0[3];
$indir .= '/' if ($indir);
$outdir .= '/' if ($outdir);
$rptdir .= '/' if ($rptdir);

if ($host eq '' || (@ARGV == 0 && $reqfile eq '')) {
  my $prg = $0;
  $prg =~ s/.*\///;
  ac_log_fatal("No host specified") if ($host eq '');
  ac_log_fatal("No request-file or output-files specified") if ($ARGV == 0 && $reqfile eq '');
  ac_log_error("Usage: $prg [-I] [-c] [-o open-tries] [-h host] [-u user] [-p passwd] [-k key] [-d dir[,dir...]] [-m max-age] [-s seconds] [-r request-file] [-R report-file] [-n file-count] [-w hh:mm|sec] [-l output-list-file] [-t strftime-spec] [-e] [output-filespec...]");
  ac_log_error("  -I: initialize cache only");
  ac_log_error("  -c: clear cache");
  ac_log_error("  -e: regex filespec");
  ac_log_error("  -t: add timestamp to output (using %Y%m%d etc.)");
  ac_log_error("  -d: remote directory and optionally request, output and report subdirectories");
  ac_log_error("  -m: only download recent files (default 7 days, max-age in hours) or only initialize old files (default 0)");
  ac_log_error("  -s: skip download if size did not change and time difference is less than x seconds (default 30s)");
  ac_log_error("  -r: upload request file");
  ac_log_error("  -R: wait for the report file to change and download report file");
  ac_log_error("  -n: wait for the amount of files downloaded or timeout reached");
  ac_log_error("  -w: wait until hh:mm or sec before starting downloads. hh >= 24 is tomorrow");
  ac_log_error("  -l: write downloaded filenames to this file");
  ac_log_error("  output-filespec should not contain a subdirectory. Multiple filespecs are possible");
  ac_log_error("  environment used: FTP_HOST, FTP_USER, FTP_PASSWD, FTP_KEY, FTP_DEBUG");
  ac_log_error("     FTP_REQ_TIMEOUT, FTP_OUT_TIMEOUT, FTP_CHANGE_TIMEOUT, FTP_STAT_SLEEP, FTP_LS_SLEEP");
  exit 1;
}

if ($reqfile ne '' && ! -e $reqfile) {
  ac_log_fatal("Request file \"$reqfile\" not found");
  exit 1;
}

if (defined $waitout) {
  if ($waitout =~ /^(\d?\d):(\d\d)$/ && $2 + 0 < 60) {
    my ($h, $m) = ($1 + 0, $2 + 0);
    my $dd = 0;
    while ($h > 23) {
      $h -= 24;
      $dd += 86400;
    }
    my @t = localtime;
    $waitout = timelocal(0, $m, $h, $t[3], $t[4], $t[5]) + $dd;
  }
  elsif ($waitout !~ /^\d+$/) {
    ac_log_fatal("Invalid output wait time '$waitout' (expect hhmm format or seconds since epoch)");
    exit 1;
  }
}


$cachefile .= ".$host.$user";
$cachefile .= ".$dir" if ($dir);
$cachefile =~ s/\/+$//;
$cachefile =~ s/\//_/g;

if ($opts{c} && -s $cachefile) {
  unlink $cachefile;
}
sub readcache {
  if (open(FH, "<$cachefile")) {
    %cache = ();
    %cachemd5 = ();
    $server_timediff = undef;
    $cachechanged = 0;
    while (<FH>) {
      # file, size, datetime, md5
      # timediff 0
      chomp;
      my @a = split("\t", $_);
      if (@a >= 3) {
        $cache{$a[0]} = "$a[1]\t$a[2]";
        $cachemd5{$a[0]} = $a[3] if ($a[3] ne '')
        #print STDERR "Cache: $a[0] = '$cache{$a[0]}', $cachemd5{$a[0]}\n";
      }
      elsif ($a[0] eq '# timediff' && $a[1] =~ /^-?\d+$/) {
        $server_timediff = $a[1] + 0;
      }
    }
    close(FH);
  }
}

# Add entry to cache and/or write cache file
sub addcache {
  my ($k, $v, $md5) = @_;
  my $fh;
  if ($k && $v) {
    return if (defined $cache{$k} && $cache{$k} eq $v && (!$md5 || $cachemd5{$k} eq $md5));
    $cache{$k} = $v;
    $cachemd5{$k} = $md5;
    $cachechanged++;
    if (open($fh, ">>$cachefile")) {
      $v .= "\t$md5" if ($md5);
      print $fh "$k\t$v\n";
      close($fh);
    }
    return;
  }
  if ($cachechanged && open($fh, ">$cachefile")) {
    $cachechanged = 0;
    print $fh "# timediff\t$server_timediff\n" if (defined $server_timediff);
    foreach (sort keys %cache) {
      next if ($_ eq '');
      my $v = $cache{$_};
      $v .= "\t$cachemd5{$_}" if ($cachemd5{$_});
      print $fh "$_\t$v\n";
    }
    close($fh);
  }
}
# Convert glob expression to regular expression
# * => .*? , ? => . , [a] => [a]
# . => \. , | => \| , ^ => \^ , { => \{, etc
sub glob2re {
  my $pat = $_[0];
  # Don't use (*PRUNE) because it does not work if you have multiple patterns (*.zip *.txt)
  $pat =~ s{([\^\$(){}+?.*|]|\\[*?\\]?)}{ $1 eq '?' ? '.' : $1 eq '*' ? '.*?' : length($1) == 2 ? $1 : "\\$1" }eg;
  return "$pat";
}

if (@ARGV) {
  my @a;
  for (@ARGV) {
    push @a, ($opts{e} ? $_ : glob2re($_));
  }
  $filespec = join("|", @a);
  $filespec = qr/\A(?:$filespec)\z/s;
}

#print STDERR "filespec: $filespec\n";
#exit 1;

sub die_ftp {
  my $str = shift || '';
  if ($sftp) {
    $str .= ": " . $sftp->error;
  }
  ac_log_fatal("$str");
  exit 1;
}

my $open_ftp_count = 0; # keep track of number of times we (re)opened the connection
sub open_ftp {
  return if (defined $sftp && defined $sftp->{_connected});
  if ($open_ftp_count > $max_open_tries) {
    ac_log_fatal("FTP connection died too many times, giving up");
    exit 1;
  }
  if ($open_ftp_count > 0) {
    ac_log_error($sftp->error) if ($sftp && $sftp->error);
    ac_log_warning("FTP connection died, trying to reconnect");
  }
  $open_ftp_count += 1;
  my %args;
  $args{port} = $port if ($port);
  $args{user} = $user if ($user);
  $args{key_path} = $keypath if ($keypath);
  $args{password} = $passwd if ($passwd);
  if (!$passwd) {
    $args{more} = [qw(-o NumberOfPasswordPrompts=0 -o PreferredAuthentications=publickey,gssapi-keyex,gssapi-with-mic)];
  }
  else {
    # $args{more} = [qw(-o PreferredAuthentications=publickey,password,keyboard-interactive)];
    $args{more} = [qw(-o PreferredAuthentications=password,keyboard-interactive -o PubkeyAuthentication=no)];
  }
  $args{more} = () if (!defined $args{more});
  push @{$args{more}}, '-o', 'StrictHostKeyChecking=no';
  if ($debug) {
    $args{more} = () if (!defined $args{more});
    push @{$args{more}}, "-vvv";
    ac_log_info "Connecting to $user\@$host:$port args: " . join(" ", @{$args{more}});
  }
  $sftp = Net::SFTP::Foreign->new($host, %args);
  ($sftp && !$sftp->error) || die_ftp("sftp \"$host\" failed");
  if (defined($dir) && $dir ne '') {
    $sftp->setcwd($dir) || die_ftp("Failed to cwd \"$dir\"");
  }
  #$sftp->binary();
}

sub tms2tm {
  my ($tms) = @_;
  if ($tms =~ /(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+)/) {
    my $tm = timelocal($6, $5, $4, $3, $2 - 1, $1);
    #print STDERR "tms2tm [tms, tm, t]: [$tms, $tm, $6, $5, $4, $3, $2, $1]\n";
    return $tm;
  }
  #print STDERR "tms2tm [tms]: [$tms]\n";
  return undef;
}

sub tm2tms {
  my ($tm, $fmt) = @_;
  $fmt = "%Y-%m-%d %H:%M:%S" if (!defined $fmt);
  return strftime($fmt, localtime($tm));
}

my $md5 = Digest::MD5->new;

sub md5callback {
  my ($sftp, $data, $offset, $size) = @_;
  $md5->add($data);
}

readcache();
my $maxmtm = 0;
if ($reqfile ne '') {
  open_ftp();
  my ($st0, $st1);
  my ($reqfd, $reqf) = $reqfile =~ /(.*\/)?([^\/]+)$/;
  my ($rptfd, $rptf) = $rptfile =~ /(.*\/)?([^\/]+)$/;
  $st0 = $sftp->stat("$rptdir$rptf") if ($rptf);
  ac_log_info("Uploading \"$reqf\"");
  my $f = "$indir$reqf";
  $sftp->put($reqfile, $f, copy_time => 0, copy_perm => 0) || die_ftp("Failed to upload \"$f\"");
  sleep 1;
  ac_log_info("File \"$reqf\" uploaded");
  $st1 = $sftp->stat($f);
  $maxmtm = $st1->mtime if ($st1);
  my $tdiff = time() - $maxmtm - 1 if (defined $maxmtm);
  if (!defined $server_timediff || $server_timediff != $tdiff) {
    $server_timediff = $tdiff;
    $cachechanged++;
  }
  if ($rptfile) {
    sleep 1;
    my $tm = time();
    my $timeout = $tm + $req_timeout;
    $f = "$rptdir$rptf";
    ac_log_info(sprintf "Waiting for \"%s\" to change  (timeout %d:%02d)", $rptf, $req_timeout/60, $req_timeout%60);
    while (1) {
      $st1 = $sftp->stat("$f");
      ac_log_info($sftp->error) if ($sftp->error and ($sftp->error != SFTP_ERR_REMOTE_STAT_FAILED or $sftp->status != SSH2_FX_NO_SUCH_FILE));
      last if ($st1 && !$st0);
      last if ($st1 && ($st1->mtime != $st0->mtime || $st1->size != $st0->size));
      last if (($tm = time()) > $timeout);
      sleep $stat_sleep;
    }
    if ($tm <= $timeout) {
      $tm = time();
      my $timeout2 = $tm + $change_timeout;
      $timeout = $timeout2 if ($timeout2 > $timeout);
      $st0 = $st1;
      my $msgstate;
      sleep $stat_sleep;
      while (1) {
        $st1 = $sftp->stat("$f");
        last if (($tm = time()) > $timeout);
        if ($st1->mtime == $st0->mtime && $st1->size == $st0->size) {
          last if ($tm > $timeout2);
        }
        else {
          ac_log_info("File \"$f\" changed") if (!$msgstate);
          $msgstate = 1;
          $timeout2 = $tm + $change_timeout;
        }
        $st0 = $st1;
        sleep $stat_sleep;
      }
      if ($tm <= $timeout) {
        ac_log_info("File \"$f\" stopped changing") if ($msgstate);
      }
      else {
        ac_log_warning("File \"$f\" changed recently while timeout happened");
      }
      my $tmpf = ".$rptf.$$";
      $tmpf =~ s/\//_/g;
      $tmpf = "$rptfd$tmpf";
      ac_log_info("Downloading \"$f\"");
      $sftp->get($f, $tmpf, copy_time => 0, copy_perm => 0) || die_ftp("Failed to download \"$f\"");
      rename($tmpf, $rptfile);
    }
    else {
      ac_log_warning("Could not detect a changing \"$f\" file");
    }
  }
}

if (!defined($filespec)) {
  addcache();
  undef $sftp;
  exit 0;
}

my $tm = time();

if ($waitout && $tm < $waitout) {
  my $tms = tm2tms($waitout);
  ac_log_info("Sleeping until $tms");
  # Close sftp connection and reset counter
  undef $sftp;
  $open_ftp_count = 0;
  addcache();   # write cache in case timediff changed
  while (1) {
    my $t = $waitout - $tm;
    last if ($t <= 0);
    ac_log_info(sprintf "Sleeping (timeout %d:%02d:%02d)", $t/3600, ($t/60)%60, $t%60);
    $t = 300 if ($t > 300);
    sleep $t;
    $tm = time();
  }
  readcache();  # reinit cache after sleep, just in case somebody modified the cache file
}

my $timeout = $tm + $out_timeout;
my $msg_timeout = $tm;
my $count;
my %skipped;

if ($listfile && -e $listfile) {
  unlink $listfile;
}

open_ftp();
ac_log_info("Retrieving files");
while (1) {
  while (1) {
    # $filespec is a Regexp. This allows for 1 single loop with multiple file specs
    my $lst = $sftp->ls($outdir, ordered => 1, wanted => $filespec);
    foreach (@$lst) {
      # print STDERR "lst: $_\n";
      my ($fn, $sz, $mtm, $perm) = ($_->{filename}, $_->{a}->size, $_->{a}->mtime, $_->{a}->perm);
      if (!S_ISREG($perm)) {
        next if ($skipped{$fn});        # print message once
        if (S_ISDIR($perm)) {
          ac_log_info("Skipping directory \"$fn\"");
        }
        elsif (S_ISLNK($perm)) {
          ac_log_info("Skipping symbolic link \"$fn\"");
        }
        else {
          ac_log_info("Skipping \"$fn\" (not a regular file)");
        }
        $skipped{$fn} = 1;
        next;
      }
      my $tms = tm2tms($mtm);
      ac_log_info("File: $fn $sz $tms") if ($debug);
      $file{$fn} = "$sz\t$tms";
      $filesize{$fn} = $sz;
      $filetime{$fn} = $mtm;
      $maxmtm = $mtm if ($mtm > $maxmtm);
    }
    last if (defined $sftp->{_connected}); # still alive ?
    open_ftp();
  }

  for my $try ((0,1)) {
    foreach my $f (sort keys %file) {
      if (!exists $cache{$f} || $cache{$f} ne $file{$f}) {
        my ($csz, $ctms) = $cache{$f} =~ /([^\t]*)\t([^\t]*)/;
        my $ctm =  tms2tm($ctms);
        if ($cache{$f} && $csz == $filesize{$f} && $ctm >= $filetime{$f} - $timediff) {
          # File size did not change and file is less than $timediff seconds newer
          # This happens with multi-hosted sftp servers (for example Bloomberg)
          # where files are copied with (slightly?) different timestamps
          if ($ctm < $filetime{$f}) {
            $cache{$f} = $file{$f};   # store latest timestamp in cache
            $cachechanged++;
          }
          next;
        }
        # printf STDERR "File $f changed: '$cache{$f}' != '$file{$f}'\n";
        if ($init_only) {
          if ($mintime && $filetime{$f} > $mintime) {
            my ($sz, $tms) = $file{$f} =~ /([^\t]*)\s([^\t]*)/;
            ac_log_info("Skipping new file \"$f\" ($tms)") if ($try == 0);
            next;
          }
          ac_log_info("Initialize cache \"$f\"") if ($try == 0);
          $cache{$f} = $file{$f} if ($file{$f} ne '');
          $cachechanged++;
          $count++;
        }
        elsif ($mintime && $filetime{$f} <= $mintime) {
          my ($sz, $tms) = $file{$f} =~ /([^\t]*)\s([^\t]*)/;
          ac_log_info("File is too old \"$f\" ($tms)") if ($try == 0);
        }
        else {
          my $st0 = $sftp->stat("$outdir$f");
          if ($st0->mtime != $filetime{$f} || $st0->size != $filesize{$f}) {
            # File changed since last ls command
            # Assume nobody is playing around with timestamps, i.e. future timestamps
            # $tdiff is negative if remote is ahead of us
            my $tdiff = ($tm = time()) - $st0->mtime;
            # Ignore if time is too far off (5 minutes). This should never happen for properly configured servers
            if ($tdiff <= 300 && $tdiff >= -300 && (!defined $server_timediff || abs($tdiff) < abs($server_timediff))) {
              $server_timediff = $tdiff;
              $cachechanged++;
            }
            $filetime{$f} = $st0->mtime;
            $filesize{$f} = $st0->size;
            next if ($try == 0);        # Skip changed file in the first try
          }
          $tm = time();
          my $timeout2 = $filetime{$f} + $change_timeout + $server_timediff;
          if ($tm < $timeout2 && $tm >= $timeout2 - 2 * $change_timeout) {
            next if ($try == 0);        # Try this file second try
            my $msgstate;
            sleep $stat_sleep;
            while (1) {
              my $st1 = $sftp->stat("$outdir$f");
              last if (($tm = time()) > $timeout);
              if ($st1->mtime == $st0->mtime && $st1->size == $st0->size) {
                last if ($tm > $timeout2);
              }
              else {
                ac_log_info("File \"$f\" changed") if (!$msgstate);
                $msgstate = 1;
                $timeout2 = $tm + $change_timeout;
              }
              $st0 = $st1;
              sleep $stat_sleep;
            }
            if ($tm <= $timeout) {
              ac_log_info("File \"$f\" stopped changing") if ($msgstate);
            }
            else {
              ac_log_warning("File \"$f\" changed recently while timeout happened");
            }
          }
          $count++;
          my $tmpf = ".$f.$$";
          while (1) {
            ac_log_info("Downloading \"$f\"");
            $md5->new;
            my $res = $sftp->get("$outdir$f", $tmpf, copy_time => 0, copy_perm => 0, callback => \&md5callback);
            if (defined $sftp->{_connected} && $res) {
              my $digest = $md5->hexdigest;
              if ($csz == $filesize{$f} && $cachemd5{$f} eq $digest) {
                ac_log_info("File \"$f\" content did not change");
                $count--;
                unlink($tmpf);
              }
              else {
                my ($fn, $ts) = ($f, $filetime{$f});
                $fn .= tm2tms($ts, $outtmspec) if ($ts && defined $outtmspec);
                rename($tmpf, $fn);
                utime $ts, $ts, $fn if ($ts);
                print "Retrieved\t$fn\n";
                if ($listfile) {
                  my $fh;
                  open $fh, ">>$listfile";
                  print $fh cwd()."/$fn\n";
                  close $fh;
                }
              }
              addcache($f, $file{$f}, $digest);
              last;
            }
            ac_log_warning("Failed to download \"$f\": " . $sftp->error);
            # print "Failed\t$f\t" . $sftp->error . "\n";
            last if (($tm = time()) > $timeout);
            open_ftp();
          }
        }
      }
    }
  }
  last if ($count >= $out_count);
  last if (($tm = time()) > $timeout);
  if ($tm >= $msg_timeout) {
    my $t = $timeout - $tm;
    $msg_timeout = $msg_timeout + 300;
    $msg_timeout = $tm + 300 if ($tm > $msg_timeout);
    my $c = $out_count - $count;
    ac_log_info(sprintf "Waiting for $c/$out_count files  (timeout %d:%02d)", $t/60, $t%60);
  }
  sleep $ls_sleep;
}

my $total = scalar(keys %file);
if (!$count) {
  if (!$total) {
    ac_log_info("No files found");
  }
  else {
    ac_log_info("No changed files found (0/$total)");
  }
}
elsif (!$init_only) {
  ac_log_info("Downloaded $count/$total files");
}
else {
  ac_log_info("Initialized $count/$total files");
}

undef $sftp;

addcache();     # Write cache

# vim:et:
