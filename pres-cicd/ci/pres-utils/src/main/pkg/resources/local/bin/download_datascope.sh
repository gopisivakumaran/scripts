#!/bin/bash
#
# 2021-03 Berend Reitsma
# Download Refinitiv datascope select files from the sftp server
# Files are downloaded in ../datascope/downloads and copied to ../datascope/
# ac_getsftp.pl is used to download new/changed files (using a cached listing file)
# 
# TODO: Use a (generated) configuration file instead of hardcoded parameters

[ -z "$AC_DATA" -o -z "$AC_WORKDIR" ] && . $HOME/.bash_profile

dir="$(cd $(dirname "$0"); pwd)"
PATH="$dir:$PATH"

vendor=datascope
DLDIR=${AC_DATA:-$HOME/data}/vendor_files/$vendor/downloads
export FTP_HOST=datafeed.asset-control.com
export FTP_USER=datafeed
export FTP_PASSWD='datafeed'
remotedir="ps_created_loaders/daily/$vendor"
outtmspec=""	# ".%Y%m%d"	# Add date to downloaded file names if set
# regex!
OUTFNSPEC=".*\.txt(?:\.[0-9]{8}.*)?"

if ! command -v ac_log_info >/dev/null 2>&1; then
  if command -v ac_utilities.sh >/dev/null 2>&1; then
    . ac_utilities.sh
  else
    PROGNAME=${0##*/}
    function ac_log_info    { echo "${AC_INFO_PREFIX:-+++INFO+++ }$(date +%Y%m%d_%H:%M:%S) $PROGNAME: $*" 1>&2; }
    function ac_log_warning { echo "${AC_WARNING_PREFIX:-+++WARNING+++ }$(date +%Y%m%d_%H:%M:%S) $PROGNAME: $*" 1>&2; }
    function ac_log_error   { echo "${AC_ERROR_PREFIX:-+++ERROR+++ }$(date +%Y%m%d_%H:%M:%S) $PROGNAME: $*" 1>&2; }
    function ac_log_fatal   { echo "${AC_FATAL_PREFIX:-+++FATAL+++ }$(date +%Y%m%d_%H:%M:%S) $PROGNAME: $*" 1>&2; }
    function check_rc       { typeset rc=$?; if [ $rc != 0 ]; then ac_log_fatal "$*"; exit $rc; fi; }
  fi
fi

LOG="$AC_WORKDIR/log/$(date "+%Y%m/$vendor.%Y%m%d.log")"
[ -t 0 ] && TEE="tee -a '$LOG' 1>&2" || TEE="cat >> '$LOG'"

mkdir -p "$DLDIR" "${LOG%/*}"
cd "$DLDIR" || exit

if :; then # redirect all output at the end of the if statement

  # Using FTP_* environment variables instead of -h, -u and -p
  ac_getsftp.pl -l .list -d "$remotedir" ${outtmspec:+-t "$outtmspec"} -e "$OUTFNSPEC"

  # Decrypt and gunzip
  if [ -s .list ]; then
    while read fn; do
      fn0=$fn
      if file -b "$fn" | grep 'uuencoded' >/dev/null 2>&1; then
        ac_log_info "decrypt $fn"
        mv "$fn" "$fn.enc"
        if des -D -u -k "$deskey" "$fn.enc" "$fn.dec"; then
          mv "$fn.dec" "$fn"
          touch -r "$fn.enc" "$fn"
        else
          fn="$fn.enc"
          ac_log_error "Failed to decrypt $fn"
        fi
      fi
      if file -b "$fn" | grep 'gzip compressed' >/dev/null 2>&1; then
        ac_log_info "gunzip $fn" 
        f="${fn//.gz./.}"
        f="${f%.gz}"
        [ "_$fn" = "_$f.gz" ] || mv "$fn" "$f.gz"
        if gunzip -f -q < "$f.gz" > "$f"; then
          fn="$f"
          touch -r "$fn.gz" "$fn"
        else
          ac_log_error "Failed to gunzip $f.gz"
        fi
      fi
      if [ "_$fn" = "_$fn0" ]; then
        cp -fp "$fn" ..
      else
        mv -f "$fn" ..
      fi
    done < .list
  fi
  ac_log_info "done"

fi 2>&1 | eval "$TEE" 
