#!/bin/bash
#
# 2021-03 Berend Reitsma
# Monitor download directory with inotifywait (default: $AC_DATA/vendor_files/$vendor)
# For each file run: ierun $interface -t $task
# The file is moved to done or failed afterwards
# Processing one file at a time

[ -z "$AC_DATA" -o -z "$AC_WORKDIR" ] && . $HOME/.bash_profile

interface=BLOOMBERG
vendor=bloomberg

. ac_utilities.sh
[ -f $AC_SYSTEM/local/files/$interface.cnf ] && . $AC_SYSTEM/local/files/$interface.cnf

inputdir="${FEEDFILES_DIR:-$AC_DATA/vendor_files/$vendor}"
lockfn="$inputdir/.monrun.lock"
#task="${task:-INPUTFILE}"

mkdir -p "$inputdir/done" "$inputdir/failed"

shlock2 -f "$lockfn" -p $$
if [ $? -ne 0 ]; then
  ac_log_fatal "Failed to get lock on '$lockfn'"
  ac_log_info "Only one single monitor process is allowed to run on $inputdir"
  exit 1
fi
trap "rm -f '$lockfn'" 0

cd "$inputdir"
check_rc "Failed to change directory to $inputdir"

# TODO: Create new log file for each day. Use perl to do the creating and redirecting...
LOG="$AC_WORKDIR/log/$(date "+%Y%m/monrun_$vendor.%Y%m%d.log")"
mkdir -p "${LOG%/*}"
[ -t 0 ] && TEE="tee -a \"\$LOG\" 1>&2" || TEE="cat >> \"\$LOG\""

process () {
  typeset f="$1"

  if [ -f "$f" ]; then
    if [ "_$f" != "_${f##*.txt.notes.txt}" -o "_$f" != "_${f##*.txt.notes.txt.*}" ]; then
      ac_log_info "Skipping and moving $f to done"
      mv "$f" "done/"
      return
    fi
    ac_log_info "Processing $f"
    INPUTFILE="$f" ierun $interface${task:+ -t "$task"}
    typeset rc=$?
    if [ "_$rc" = _0 ]; then
      ac_log_info "File $f moved to done"
      mv "$f" "done/"
    else
      ac_log_info "File $f moved to failed"
      mv "$f" "failed/"
    fi
  elif [ -e "$f" -a ! -d "$f" ]; then
    ac_log_info "Skipping $f"
  fi
}

if :; then # redirect all output at the end of the if statement

  ( echo "START"
  inotifywait -q -m -e close_write -e moved_to --exclude '/\.|/shlock[0-9]*$' --format "%e %f" . ) | while read ev fn; do
    # ac_log_info "ev=$ev fn=$fn"
    if [ "_$ev" = _START ]; then
      sleep 1
      ls -1
    else
      echo "$fn"
    fi | while read f; do
      process "$f"
    done
  done

fi 2>&1 | eval "$TEE"

