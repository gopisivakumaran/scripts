#!/bin/bash

h1=${1}

curl --location --request PATCH http://ac-demo-1.acx-sandbox.net:9020/select/api/requests/${h1} \
--header 'Content-Type: application/json' \
--data '{
   "id":'${h1}',
    "instruments": [
        {
            "identifier": "'${2}'\t",
            "identifierType": "BB_GLOBAL",
            "exchangeCode": "",
            "pricingSource": ""
        }
    ]
}' | jq

curl --location --request POST http://ac-demo-1.acx-sandbox.net:9020/select/api/submissions/${h1} \
--header 'Content-Type: application/json' \
--data '{
   "id":'${h1}',
   "status":"SUBMITTED"
}' | jq
