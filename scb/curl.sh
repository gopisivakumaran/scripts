#!/bin/bash

endpoint="https://fastapi.uat01.acx-sandbox.net"

cache(){
	curl -X PUT ${endpoint}/v1/xref/${1}/ado/${2} -H "accept: application/json"
}

curl -X PUT ${endpoint}/clear-cache -H "accept: application/json"

n=1000000
c=10

for ((i=1;i<=${c};i++))
do
	s=$(($((${i}-1)) * $((${n}/${c})) + 1))
	f=$((${n}/${c} * ${i}))
	for((j=${s};j<=${f};j++))
	do
		cache ${j} $((${j} * 5)) 
	done &	
done >/dev/null 2>&1

n1=`curl ${endpoint}/health | jq | grep xref -A5 | grep size | awk '{print $2}' | sed 's/.\{1\}$//'`
sleep 10
n2=`curl ${endpoint}/health | jq | grep xref -A5 | grep size | awk '{print $2}' | sed 's/.\{1\}$//'`

echo $n1
echo $n2
echo "speed is $(($(($n2 - $n1))/10)) entries per second"
