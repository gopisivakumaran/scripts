#!/bin/bash

n=300000
c=300
txt=fastapi.json
dir=json

sed -i '$d' ${txt}
sed -i '$d' ${txt}
sed -i '2d' ${txt}
sed -i '1d' ${txt}

for ((i=1;i<=${c};i++))
do
	f=$((${n}/${c} * ${i}))
	head -n ${f} ${txt} | tail -n $((${n}/${c})) > ${dir}/fastapi_${f}.json
	sed -i '1s;^;\"xrefIdAdoId\": {\n;' ${dir}/fastapi_${f}.json
	sed -i '1s;^;{\n;' ${dir}/fastapi_${f}.json
	l=`cat ${dir}/fastapi_${f}.json | wc -l` 
	sed -i ${l}s/.$// ${dir}/fastapi_${f}.json
	echo "}" >> ${dir}/fastapi_${f}.json
	echo "}" >> ${dir}/fastapi_${f}.json 
done

for file in `ls ${dir}`
do
curl -X PUT "https://fastapi.uat01.acx-sandbox.net/v1/xref" -H "accept: application/json" -H "Content-Type: application/json" -d @${dir}/${file}
done
