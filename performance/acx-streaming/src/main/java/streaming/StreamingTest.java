package stream;

import com.assetcontrol.acx.streaming.client.StreamingClient;
import java.util.Properties;
import com.assetcontrol.acx.api.query.Query;
import com.assetcontrol.acx.streaming.client.StreamingClient;
import java.time.Instant;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;
import java.lang.InterruptedException;
import java.util.concurrent.ExecutionException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StreamingTest {

	public static void main(String[] args) 
			throws InterruptedException, ExecutionException {
		
		//set server properties
		Properties serverproperties = new Properties();
		serverproperties.setProperty("servers", "172.20.35.245:50051");

		//initialize streaming client
		StreamingClient client = new StreamingClient(serverproperties);
		client.initialize(); client.login("streamer", "streamerpass");
	
		//create thread
		Future<?> filter = ForkJoinPool.commonPool().submit(() -> {
		Query<String> result = client.connection().ados(
				"C0_I0030 like \"10.%\"", Instant.now()
		);

		for (String adoid : result.getResults()) {
			System.out.print(currentTime() + "\t" + ":" + "\t" + adoid + "\n");
		}
		});

		//output results
		filter.get();
	}

	public static String currentTime() {
    		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date());
	}
}
