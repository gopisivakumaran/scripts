###############################################################################
# RUN TIME BEHAVIOR
# These are properties you typically want to set per interface in 
# $AC_SYSTEM/interfaces/<interface_name>/config/<interface_name>.properties
###############################################################################

# Mandatory.
# The default number of parallel threads to be used in the Interface. This is
# only relevant if the specific Interface supports multi-threading. Note that
# the Jobprovider can override this option on a per-job basis with the
# property job.flowthreadcount.
ie4.flowthreadcount = 2

# Optional.
# File name of the Job Provider script.  The ierun script looks for the
# jobprovider in the directory: $AC_SYSTEM/interfaces/<interface>/bin
# Default value is "jobprovider". 
#ie4.config.jobprovider = <jobprovider-script>

# Optional.
# The full path to the pre-job script. This is executed before the job is
# being run. This pre-job script takes one job-description file as an
# argument.
# There is no default; when omitted no pre-job script is executed.
#ie4.job.precommand = <path/to/pre-job-script>

# Optional.
# The full path to the post-job script. This is executed after the job is run.
# This post-job script takes the job-description file and a string indicating
# the success or failure of the jobprocessor as its arguments.
# There is no default; when omitted no post-job script is executed.
#ie4.job.postcommand = <path/to/post-job-script>

# Optional.
# Activate the next line if you want to enable direct normalization. 
# Important! This property should only be set if this interface comes with
# NORMALIZATION_N0 2.0.
# Default value is false. 
#ie4.normalization.enabled = true

# Optional.
# The reader produces data-blocks which are in turn processed by one or more
# flows. If the reader can produce data-blocks quicker than the flow(s) can
# process. Waiting data-blocks are stored in memory. The flushinterval
# property is used to determine the number of data-blocks waiting to be
# processed. After this number is reached the reader waits with producing new
# data-blocks, so the flow(s) can process the data-blocks that are in memory.
# Default value is 1000.
#ie4.flushinterval = 1000

# Optional.
# If true, then in case of a RuntimeException, the job will continue and only
# a message is logged.  Default value false.
#ie4.processing.nofail = false

# Optional.
# Used by the jobprovider. The date is the assumed batch run date in the
# format YYYYMMDD.  If not given, today is assumed.
#ie4.run.date = <YYYYMMDD>

# Optional.
# The full class name of custom message handler implementation
#ie4.log.client.message.handler = <com.ac.dms.library.logging.handler.MessageHandler implementation>

# Optional.
# Activate the next line if you want log messages to appear on standard output.
# Default value is false.
#ie4.log.console = true

# Optional.
# Specifies the format for the logging. 
# Use {x} to fill in attributes of the message, where x can be:
# 0 - Handler name
# 1 - System time (formated using the specified dateFormat)
# 2 - Bundle name
# 3 - Message id
# 4 - Message type
# 5 - Message context
# 6 - Formatted Description 
# Default is "{4}{5} {1} {7}/{8} {2}.{3} {6}"
#ie4.log.format = "{4}{5} {1} {7}/{8} {2}.{3} {6}"

###############################################################################
# DON'T CHANGE ANYTHING BELOW THIS LINE
###############################################################################

# The application to run
ie4.run.application.id = CONSOLIDATION_C0

