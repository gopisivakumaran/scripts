#!/usr/bin/bash

cat << EOF | ac_evalform -f -
local l_ados=attribute('C0.LST.00-48144','C0_SRC01');
if (!is_list(l_ados)) l_ados=[l_ados];
local i; 
for (i=0; i<len(l_ados);i++)
{
	if(len(l_ados) == 1)
		l_ados[i] = elt ( l_ados[i], [3, len(l_ados[i]) ]);
	else if(len(l_ados) == 2)
		l_ados = elt ( l_ados[0], [3, len(l_ados[0]) ]);
	else if(len(l_ados) == 3)
                l_ados = elt ( l_ados[0], [3, len(l_ados[0]) ]);
};
return l_ados;
EOF
