#!/bin/ksh

#-------------------------------------
# Source logging utilities
#-------------------------------------
. ac_utilities.sh

ac_log_info "...Start Postjobaction..."

cmd="${0##*/}"

if [ $# -lt 2 ]; then
  echo "Usage: $cmd job-file status msg..." 1>&2
  echo "       status: SUCCESS or FAILED" 1>&2
  exit 1
fi

#-------------------------------------
# Variable definition
#-------------------------------------
jobdef="${1:-}"
result="${2:-}"
shift 2
rc=0

# tmp files used to update C0 ADOs
tmpfn="$ie4_run_batchdir/postjobaction.$$"
symbolfn="$ie4_run_batchdir/postjobaction.ado"
adofn="$ie4_run_batchdir/postjobaction.c0.ado"
amdprfn="$ie4_run_batchdir/postjobaction.amdpr"
nbrthreads=6

# construct amdpr string set dervied DFAs
AWK_PRG1='{
  nbr=split(ATTR, array, ",")
  cnt=(2 + nbr)
  printf "%s,%s,F,A,2,32,3,%d,DATE,TIME,%s,7,\"%s\"\n", TREEID, $1, cnt, ATTR, VALFN

}'
  # original line in the AWK_PRG1 above
  #printf "%s,%s,F,A,3,%d,DATE,TIME,%s,31,%d,%s\n", TREEID, $1, cnt, ATTR, nbr, ATTR
#-------------------------------------
#  General Functions
#-------------------------------------
function enable_derived_dfa {

  dfa="C0#BID,C0#MID,C0#ASK"  # comma list of attributes to enable as derived
  treeid="C0_EOD"  # default treeid
  validation_fn="Price_Percentage_Outlier_check(5);Price_Stale_Check(5);Price_Zero();Price_Non_Positive();Price_Fill_NA();"

  ac_log_info "......enabling derived datafile attributes..."
  listado C0_BB_TDS_ADOS | sort -u > $symbolfn 2> /dev/null
  if [ -s "$symbolfn" ]; then
    comm -12 $symbolfn $tmpfn > $adofn
    if [ -s "$adofn" ] ; then
      ac_log_info ".........applying updates..."
    # cat $adofn | gawk -F"|" -v ATTR="$dfa" -v TREEID="$treeid" "$AWK_PRG1" > $amdprfn
      cat $adofn | gawk -F"|" -v ATTR="$dfa" -v TREEID="$treeid" -v VALFN="$validation_fn" "$AWK_PRG1" > $amdprfn
      if [ -s "$amdprfn" ] ; then
        amd_pr -AEfZ $amdprfn -n $nbrthreads
      fi
    fi
  fi
}

#-------------------------------------
#  Init
#-------------------------------------
timestamp=`date '+%Y%m%d-%H%M%S'`

if [ -s "$jobdef.log" ] ; then
  perl -ne '{ printf "$1\n" if (m/^.*C0: \[acAdoIDs=\[(.*)\], entityName=(.*)\],.*/); }' "$jobdef.log" >> $tmpfn
  if [ -s "$tmpfn" ] ; then
    ac_log_info "......new consolidated ADOS detected..."
    enable_derived_dfa
    check_rc "Failed." 1
    ac_log_info "......done."
  fi
fi

ac_log_info "...Finished."

exit 0

