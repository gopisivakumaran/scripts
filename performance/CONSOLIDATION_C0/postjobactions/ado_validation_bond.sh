#!/usr/bin/bash

for s in `listado C0_BOND_ADOS`;
do
cat << EOF | ac_bl -
TRANS NOFAIL
OBJECT ADO
ADD symbol="${s}"
OBJECT ADO_ATTRIBUTE
ADD symbol="${s}" attrid="\$ADORULES" value="BOND" status=1
EOF
done

