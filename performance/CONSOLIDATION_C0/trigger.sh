#!/bin/ksh

. ac_utilities.sh

USAGE='trigger.sh - script that outputs acbl for retrigging an attribute for one or more ADOs
-s ado symbol 
-l ado list 
-a attribute

Example usage:
./trigger.sh -l C0_ADO -a C0_I0054
./trigger.sh -s C0.LST.12 -a C0_I0045
'

while getopts "s:a:l:" opt; do
  case $opt in
  s)
     export SYMBOL="$OPTARG"
     ;;
  a)
     export ATTRID="$OPTARG"
     ;;
  l) export ADOLIST="$OPTARG"
     ;;
  esac
done

if [ $# -lt 1 ]; then
    ac_log_error "$USAGE"
    exit 1
fi

if [ -z "$ATTRID" ]; then
  ac_log_error "Attribute not given with -a"
  ac_log_error "$USAGE"
  exit 1
fi

AWKPRG='
{
  print "TRANS"
  print "OBJECT ADO TRIGGER symbol=\""$1"\" attrid=\""ATTRID"\""
}'

if [ ! -z "$ADOLIST" ]; then
  listado "$ADOLIST" | $AC_AWK -v ATTRID="$ATTRID" "$AWKPRG" -
else
  if [ ! -z "$SYMBOL" ]; then
    echo "$SYMBOL" | $AC_AWK -v ATTRID="$ATTRID" "$AWKPRG" -
  else
    ac_log_error "Ado symbol not given with -s or no Ado list given with -l"
    ac_log_error "$USAGE"
    exit 1
  fi
fi
