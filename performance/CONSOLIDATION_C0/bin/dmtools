#!/usr/bin/ksh
#------------------------------------------------------------------------------
# Script for running the DatamodelTool class.
#
# This script expects to find the java libraries (jars) in the directory
# ../lib.  All jar-files in this directory are added to the classpath.
#
# Usage:
#   dmtools [-X jvmoptions] [-D javaproperties] ...
#
# Options:
#   -X jvmoptions   Specify java virtual machine options.
#   -D javaprops    Specify java property, example: -Dlog.console=true
#   -help           Show command-line help.
#
# Exit codes:
#   0               If no jobs have failed.
#   1               Program execution failed.
#   2               Error in commandline arguments.
#
# The following environment variables can be used to override default
# configuration:
#   EXTRA_JVMOPTS   Additional java-VM options to use (-X and -D can also be
#                   set as options to this script).
#   EXTRA_CLASSPATH The contents of this variable is prepended to the 
#                   classpath.
#   NOOPT           If set, does not start anything but prints java command
#                   line on stdout.
#
# (C) 2010, Asset Control NV. All rights reserved.
#
# $$Id
#------------------------------------------------------------------------------
set -u
prgname=${0##*/}
prgdir=$(dirname "$0")
alias local=typeset

log() {
    local level=$1
    shift
    echo "$(date '+%Y-%m-%d %T') $prgname $level $*" >&2
}

warning() {
    log "WARN " "$*"
}

fatal() {
    log "FATAL" "$*"
    exit 1
}

#------------------------------------------------------------------------------
# class name with main method 
class="com.ac.dms.application.ie4.DatamodelTool"
jvmopts="${EXTRA_JVMOPTS:-}"

while getopts ":X:D:" opt
do
    case "$opt" in
    X)  jvmopts="$jvmopts -X$OPTARG"
        ;;
    D)  jvmopts="$jvmopts -D$OPTARG"
        ;;
    *)  OPTIND=$((OPTIND - 1))
        # we assume other options are for the java application...
        break
        ;;
    esac
done

shift $((OPTIND - 1))

#------------------------------------------------------------------------------
# main

libpath="$prgdir/../lib"
classpath=${EXTRA_CLASSPATH:-}

if [[ -d "$libpath" ]]
then
    for jar in "$libpath"/*.jar
    do
        classpath="${classpath:+${classpath}:}$jar"
    done
else
    if [[ -n "${EXTRA_CLASSPATH:-}" ]]
    then
        warning "Java libraries not found: $libpath"
    else
        fatal "Java libraries not found: $libpath"
    fi
fi

if [[ -n "${EXTRA_CLASSPATH:-}" ]]
then
    warning "Using EXTRA_CLASSPATH=$EXTRA_CLASSPATH"
fi

rc=0
${NOOPT:+echo} java $jvmopts -cp "$classpath" "$class" ${@+"$@"} || rc=$?

exit $rc
