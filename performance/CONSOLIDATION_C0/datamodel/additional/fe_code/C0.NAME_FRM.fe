global string entityName = "entityName";
global string src25 = $NA;
/*
 * in the MT_CONFIGURATION table we have to look up so called entities, below you can see an example of such. From the
 * entity we are interested in its name(id), longname(name). In this function a hash is created with <key, value> as
 * <name, longname>, however the longname contains a N0-attribute which should be reprefixed to C0. The entity starts at
 * the row where the word 'entity' is in the first column The end of an entity is the row where column1 is also filled
 * and is preceded by no values in column1. this is how the entity occurs in the table column1 column2 column3 entity
 * name LST_EQY type LST symbol C0.LST longname N0_I0002 template C0_LI_T001_LSA Comment For EQUITY //end of entity
 */
function createHashFromTable()
{
    local list a = load("MT_CONFIGURATION", "TABTABLE");
    local integer isEntity = 0;
    hash_delete(entityName); // create a new hash
    local i;
    for (i = 0; i < len(a); i++)
    {
        if (isEntity && a[i][0] == "" && a[i][1] == "longname" && src25 != $NA)
        {     // we are 'inside' entity at the row with the longname (needed as value in the hash)
            local string name = a[i][2];
            if (elt(name, [0, 1]) == "N0")
            {
                name = "C0" + elt(name, [2, len(name) - 1]);
                hash_put(entityName, src25, name);
                src25 = $NA;
            }
            else
            {
                $STATUS = 2; // suspect value status
                print("wrong value in table 'MT_CONFIGURATION' in second column: " + str(name) +
                        "should always be a N0-attribute");
            }
        }
        else if (isEntity && a[i][0] != "" && a[i][0] != "entity")
        {     // end of entity
            isEntity = 0;
        }
        else if (a[i][0] == "entity" && a[i][1] == "name")
        {     // we are now inside entity, get the value from the name(id) row and use that later as key
            isEntity = 1;
            src25 = a[i][2];
            if (hash_get(entityName, src25) != $NA)
            {
                print("duplicate value in table 'MT_CONFIGURATION' for name: " + str(src25));
            }
        }
    }
}

global integer initialized = 0;

/*
 * retrieve for the current symbol, the value of C0_SRC25 and look that up in the hash. The lookup value is also a
 * C0-Attribute which value should be calculated on this symbol
 * 
 * For Account data we make an exception to construct the name. This data is not matched so the name and template
 * configuration are not configured in the MT_CONFIGURATION table.
 */
function select()
{
    local string c0_src04 = attribute($SYMBOL, "C0_SRC04"); // get value for C0_SRC04
    if (c0_src04 == 'E')
    {     // 'E' means this is Account data so return Account name
        local string name = "UNKNOWN LONGNAME"; // The default name
        local list name_attributes = attribute($SYMBOL, ["C0_AC004", "C0_AC001"]); // ACCOUNT LONG NAME, ACCOUNT ID
        if (name_attributes[0] != $NA)
        {
            name = name_attributes[0];
        }
        else if (name_attributes[1] != $NA)
        {
            name = name_attributes[1];
        }
        return name;
    }
    if (!initialized)
    {     // make sure to create the hash only once
        createHashFromTable();
        initialized = 1;
    }
    local string c0_src25 = attribute($SYMBOL, "C0_SRC25"); // get the C0_SRC25 for this symbol
    local string nameAttribute = hash_get(entityName, c0_src25); // should always return a C0-attribute
    return (nameAttribute == $NA) ? $NA : attribute($SYMBOL, nameAttribute); // return the value for the C0-attribute

}