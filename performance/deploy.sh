#!/usr/bin/bash

dir=$(pwd $0)
export HOST=
export KAFKA_HOST=

[[ -n "$AC_SYSTEM" ]] || echo "AC System directory not set"; exit 1
[[ -d "$AC_SYSTEM/interfaces/BLOOMBERG" ]] || echo "Bloomberg interface is not installed"; exit 1
[[ -d "$AC_SYSTEM/interfaces/NORMALIZATION_N0" ]] || echo "Normalization interface is not installed"; exit 1
[[ -d "$AC_SYSTEM/interfaces/CONSOLIDATION_C0" ]] || echo "Consolidation interface is not installed"; exit 1

echo "Unpacking configurations into $AC_SYSTEM"
cp -r BLOOMBERG/config $AC_SYSTEM/interfaces/BLOOMBERG
cp -r BLOOMBERG/feedfiles $AC_SYSTEM/interfaces/BLOOMBERG

cp -r CONSOLIDATION_C0/config $AC_SYSTEM/interfaces/CONSOLIDATION_C0
cp -r CONSOLIDATION_C0/bin $AC_SYSTEM/interfaces/CONSOLIDATION_C0
cp -r CONSOLIDATION_C0/datamodel $AC_SYSTEM/interfaces/CONSOLIDATION_C0
cp -r CONSOLIDATION_C0/scale $AC_SYSTEM/interfaces/CONSOLIDATION_C0

echo "Installing Consolidation datamodel"
$AC_SYSTEM/interfaces/CONSOLIDATION_C0/bin/installdm;
$AC_SYSTEM/interfaces/CONSOLIDATION_C0/bin/installdm -u

echo "Installing ElasticSearch Logstash Kibana stack"
cd ${dir}/elk;
docker-compose up -d

echo "Installing filebeat"
sudo yum install filebeat

echo "Unpacking filebeat configuration"
cd ${dir}/filebeat; sudo cp filebeat.yml /etc/filebeat

echo "Installing rundeck"
rpm -Uvh http://repo.rundeck.org/latest.rpm
sudo yum install rundeck java

echo "Configuring rundeck"

echo "Starting rundeck"
sudo service rundeckd start

echo "Installing rundeck cli"
wget https://bintray.com/rundeck/rundeck-rpm/rpm -O bintray.repo
sudo mv bintray.repo /etc/yum.repos.d/
sudo yum install rundeck-cli

echo "Creating rundeck project"
rd projects create -p "Performance"

echo "Upload rundeck job definitions"
rd jobs load -r -f rundeck/create_instruments_job.yaml -F yaml -p "Performance"
rd jobs load -r -f rundeck/delete_instruments_job.yaml -F yaml -p "Performance"
