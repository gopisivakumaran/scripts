#!/bin/bash


usage(){
echo -e "usage:\n${0}
  -t <path to template file>
  -p <programme name, e.g. getData>
  -r <range for getHistory>
  -f <programme flag, e.g. adhoc>
  -c <download categories, e.g. Security Master>
  -i <file containing list of identifier|identifierType>
  -n <request name>
";
}

if [ $# -eq 0 ]
then usage;
exit 0; fi

while getopts "t:p:r:f:c:i" opt; do
  case ${opt} in
    t ) template=${OPTARG}
      ;;
    p ) progname=${OPTARG}
      ;;
    r ) historyRange=${OPTARG}
      ;;
    f ) progflag=${OPTARG}
      ;;
    c ) category=${OPTARG}
      ;;
    i ) identifiers=${OPTARG}
      ;;
    n ) name=${OPTARG}
      ;;	 
    \? ) usage
      ;;
  esac
done
shift $((${OPTIND} -1))

reqId=`curl --location --request POST 'http://vm-act-med-prd-app-01:9020/select/api/requests' \
--header 'Content-Type: application/json' \
--data '{
   "name":"'${name}'",
   "dataSource":{
      "id":"BB"
   }
}' | jq '.id'

fn=attributes.txt
[[ -f ${fn} ]] && rm ${fn}
cat << EOF >> ${fn}
{
   "id": ${reqId},
   "attributes":[
EOF

for l in ${template}
do
echo "{" >> ${fn}
echo '"id":"'${l}'"' >> ${fn}
echo "}," >> ${fn}
done
cat << EOF >> ${fn}
  ]
}
EOF
count=$((cat ${fn} | wc -l))
sed -i $((${count}-2))s/.$// ${fn}

curl --location --request PATCH http://vm-act-med-prd-app-01:9020/select/api/requests/${reqId} \
--header 'Content-Type: application/json' \
--data @attributes.txt

curl --location --request PATCH http://vm-act-med-prd-app-01:9020/select/api/requests/${reqId} \
--header 'Content-Type: application/json' \
--data '{
   "id":'${reqId}',
   "propertyValues":[
      {
         "id":1,
         "value":"'${progname}'",
         "valueId":1,
         "name":"PROGRAMNAME",
         "type":"single"
      },
      {
         "id":2,
         "value":"'${category}'",
         "valueId":3,
         "name":"downloadCategories",
        "type":"array"
      },
      {
         "id":5,
         "value":"'${progflag}'",
         "valueId":27,
         "name":"PROGRAMFLAG",
         "type":"single"
      }
   ]
}' | jq

curl --location --request POST http://vm-act-med-prd-app-01:9020/select/api/submissions/${reqId} \
--header 'Content-Type: application/json' \
--data '{
   "id":'${reqId}',
   "status":"SUBMITTED"
}'
