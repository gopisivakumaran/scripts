#!/bin/bash


usage(){
echo -e "usage:\n${0}
  -t <path to template file>
  -p <programme name, e.g. getData>
  -r <range for getHistory>
  -f <programme flag, e.g. adhoc>
  -c <path to file containing list of download categories>
  -n <request name>
";
}

if [ $# -eq 0 ]
then usage;
exit 0; fi

while getopts "t:p:r:f:c:i:n:" opt; do
  case ${opt} in
    t ) template=${OPTARG}
      ;;
    p ) progname=${OPTARG}
      ;;
    r ) historyRange=${OPTARG}
      ;;
    f ) progflag=${OPTARG}
      ;;
    c ) categories=${OPTARG}
      ;;
    n ) name=${OPTARG}
      ;;	 
    \? ) usage
      ;;
  esac
done
shift $((${OPTIND} -1))

reqId=`curl --location --request POST 'http://vm-act-med-prd-app-01:9020/select/api/requests' \
--header 'Content-Type: application/json' \
--data '{
   "name":"'${name}'",
   "dataSource":{
      "id":"BB"
   }
}' | jq '.id'`

tmp=template.txt
[[ -f ${tmp} ]] && rm ${tmp}
cat << EOF >> ${tmp}
{
   "id": ${reqId},
   "attributes":[
EOF

for line in `cat ${template}`
do
echo "{" >> ${tmp}
echo '"id":"'${line}'"' >> ${tmp}
echo "}," >> ${tmp}
done
cat << EOF >> ${tmp}
  ]
}
EOF
count=$(cat ${tmp} | wc -l)
sed -i $((${count}-2))s/.$// ${tmp}

curl --location --request PATCH http://vm-act-med-prd-app-01:9020/select/api/requests/${reqId} \
--header 'Content-Type: application/json' \
--data @${tmp} | jq
rm ${tmp}

tmp=properties.txt
[[ -f ${tmp} ]] && rm ${tmp}
cat << EOF >> ${tmp}
{
"id":${reqId},
   "propertyValues":[
      {
         "id":1,
         "value":"${progname}",
         "valueId":1,
         "name":"PROGRAMNAME",
         "type":"single"
      },
EOF

for line in `cat ${categories}`
do
cat << EOF >> ${tmp}
{
      "id":2,
      "value":"${line}",
      "valueId":3,
      "name":"downloadCategories",
      "type":"array"
},
EOF
done


cat << EOF >> ${tmp}
{
      "id":5,
      "value":"${progflag}",
      "valueId":27,
      "name":"PROGRAMFLAG",
      "type":"single"
},
{
      "id":30,
      "value":86400000,
      "valueId":null,
      "name":"ELAPSEDTIME",
      "type":"single"
}
]
}
EOF

curl --location --request PATCH http://vm-act-med-prd-app-01:9020/select/api/requests/${reqId} \
--header 'Content-Type: application/json' \
--data @${tmp} | jq
rm ${tmp}



