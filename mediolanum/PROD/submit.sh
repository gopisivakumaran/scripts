#!/bin/bash

curl --location --request POST 'http://vm-act-med-prd-app-01:9020/select/api/public/v1/request/execute' \
--header 'Content-Type: application/json' \
--data '{
    "datasource": "BLOOMBERG",
    "request": "testgk1007_1",
    "instruments": [
        {
            "instrumentType": "ISIN",
            "instrumentId": "NL0000313286",
                     "exchange": {
                "code": "NA"
            },
                     "pricingSource": {
                "code": ""
            }
        },
        {
            "instrumentType": "ISIN",
            "instrumentId": "NL0000018034",
             "exchange": {
                "code": "NA"
            },
                     "pricingSource": {
                "code": ""
            }
        }
        ],
    "settings": {
        "expectedDuration": {
            "time": 10,
            "unit": "MINUTES"
        }
    }
}'

