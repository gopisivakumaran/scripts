#!/bin/bash

curl --location --request POST 'http://ops360stable:9020/select/api/public/v1/request/execute' \
--header 'Content-Type: application/json' \
--data-raw '{
    "datasource": "BLOOMBERG",
    "request": "test18",
    "instruments": [
        {
            "instrumentType": "ISIN",
            "instrumentId": "NL0000313286",
                     "exchange": {
                "code": "NA"
            },
                     "pricingSource": {
                "code": ""
            }
        },
        {
            "instrumentType": "ISIN",
            "instrumentId": "NL0000018034",
             "exchange": {
                "code": "NA"
            },
                     "pricingSource": {
                "code": ""
            }
        }
        ],
    "settings": {
        "expectedDuration": {
            "time": 10,
            "unit": "MINUTES"
        }
    }
}'
