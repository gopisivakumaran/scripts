#!/usr/bin/bash

usage(){ echo "usage"; }

if [ $# -eq 0 ]
then usage;
exit 0; fi

while getopts "h:t:" opt; do
  case ${opt} in
    h ) a=${OPTARG}
      ;;
    t ) b=${OPTARG}
      ;;
    \? ) usage
      ;;
  esac
done
shift $(($OPTIND -1))

c=$1

echo $a $b $c
