#!/bin/bash

name=`date | cut -d" " -f1`.`uuidgen | cut -d- -f4`

reqId=`curl --location --request POST 'http://vm-act-med-prd-app-01:9020/select/api/requests' \
--header 'Content-Type: application/json' \
--data '{
   "name":"'${name}'",
   "dataSource":{
      "id":"BB"
   }
}' | jq | grep id | head -1 | awk '{print $2}' | sed 's/.\{1\}$//'`

fn=template.txt
[[ -f ${fn} ]] && rm ${fn}
cat << EOF >> ${fn}
{
   "id": ${reqId},
   "attributes":[
EOF

for l in `cat all_attr.out`
do
echo "{" >> ${fn}
echo '"id":"'${l}'"' >> ${fn}
echo "}," >> ${fn}
done
cat << EOF >> ${fn}
  ]
}
EOF
count=`cat template.txt | wc -l`
sed -i $((${count}-2))s/.$// template.txt

curl --location --request PATCH http://vm-act-med-prd-app-01:9020/select/api/requests/${reqId} \
--header 'Content-Type: application/json' \
--data @template.txt

curl --location --request PATCH http://vm-act-med-prd-app-01:9020/select/api/requests/${reqId} \
--header 'Content-Type: application/json' \
--data '{
   "id":'${reqId}',
    "instruments": [
        {
            "identifier": "BBG000BF46Y8",
            "identifierType": "BB_GLOBAL",
            "exchangeCode": "",
            "pricingSource": ""
        }
    ]
}' | jq

curl --location --request PATCH http://vm-act-med-prd-app-01:9020/select/api/requests/${reqId} \
--header 'Content-Type: application/json' \
--data '{
   "id":'${reqId}',
   "propertyValues":[
      {
         "id":1,
         "value":"getData",
         "valueId":1,
         "name":"PROGRAMNAME",
         "type":"single"
      },
      {
         "id":2,
         "value":"ESTIMATES",
         "valueId":3,
         "name":"downloadCategories",
        "type":"array"
      },
      {
         "id":5,
         "value":"adhoc",
         "valueId":27,
         "name":"PROGRAMFLAG",
         "type":"single"
      }
   ]
}' | jq

curl --location --request POST http://vm-act-med-prd-app-01:9020/select/api/submissions/${reqId} \
--header 'Content-Type: application/json' \
--data '{
   "id":'${reqId}',
   "status":"SUBMITTED"
}'

