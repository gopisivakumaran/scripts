#!/bin/bash

h1=${1}

curl --location --request PATCH http://vm-act-med-prd-app-01:9020/select/api/requests/${h1} \
--header 'Content-Type: application/json' \
--data '{
   "id":'${h1}',
    "instruments": [
        {
            "identifier": "'${2}'",
            "identifierType": "BB_GLOBAL",
            "exchangeCode": "",
            "pricingSource": ""
        }
    ]
}' | jq

curl --location --request POST http://vm-act-med-prd-app-01:9020/select/api/submissions/${h1} \
--header 'Content-Type: application/json' \
--data '{
   "id":'${h1}',
   "status":"SUBMITTED"
}' | jq

