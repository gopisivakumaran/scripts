#!/bin/bash

name=`date +%d%m`.`uuidgen | cut -d- -f5`
num=`shuf -i 10000-99999 | head -1`

h1=`curl --location --request POST 'http://ac-demo-1.acx-sandbox.net:9020/select/api/requests' \
--header 'Content-Type: application/json' \
--data '{
   "name":"'${name}'",
   "dataSource":{
      "id":"BB"
   }
}' | jq | grep id | head -1 | awk '{print $2}' | sed 's/.\{1\}$//'`

echo $h1
curl --location --request PATCH http://ac-demo-1.acx-sandbox.net:9020/select/api/requests/${h1} \
--header 'Content-Type: application/json' \
--data '{
   "id":'${h1}',
   "attributes":[
      {
        "id":"BB_DS996",
        "translationCode":"MUNI_ISSUE_TYP",
        "category":"STATIC",
        "description":"MUNICIPAL ISSUE TYPE",
        "mandatory":false
     },
     {
        "id":"BB_RR405",
        "translationCode":"INS_CLAIM_CHRG_TO_TOT_REV",
        "category":"STATIC",
        "description":"INSURANCE CLAIMS/CHARGES TO TOTAL REVENUE",
        "mandatory":false
     }
  ]
}' | jq

curl --location --request PATCH http://ac-demo-1.acx-sandbox.net:9020/select/api/requests/${h1} \
--header 'Content-Type: application/json' \
--data '{
   "id":'${h1}',
    "instruments": [
        {
            "identifier": "BBG000BF46Y8\t",
            "identifierType": "BB_GLOBAL",
            "exchangeCode": "",
            "pricingSource": ""
        }
    ]
}' | jq

curl --location --request PATCH http://ac-demo-1.acx-sandbox.net:9020/select/api/requests/${h1} \
--header 'Content-Type: application/json' \
--data '{
   "id":'${h1}',
   "propertyValues":[
      {
         "id":1,
         "value":"getData",
         "valueId":1,
         "name":"PROGRAMNAME",
         "type":"single"
      },
      {
         "id":2,
         "value":"ESTIMATES",
         "valueId":3,
         "name":"downloadCategories",
        "type":"array"
      },
      {
         "id":5,
         "value":"adhoc",
         "valueId":27,
         "name":"PROGRAMFLAG",
         "type":"single"
      }
   ]
}' | jq

curl --location --request POST http://ac-demo-1.acx-sandbox.net:9020/select/api/submissions/${h1} \
--header 'Content-Type: application/json' \
--data '{
   "id":'${h1}',
   "status":"SUBMITTED"
}'

