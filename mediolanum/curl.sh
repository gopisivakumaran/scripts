#!/bin/bash


## output usage
usage(){ echo "${0} <id> <file containing identifier|identifierType> <default identifier type>"; }

if [ $# -eq 0 ]
then usage;
exit 0; fi


## submit function
submit(){
curl --location --request PATCH http://vm-act-med-prd-app-01:9020/select/api/requests/${h1} \
--header 'Content-Type: application/json' \
--data '{
   "id":'${1}',
    "instruments": [
        {
            "identifier": "'${2}'",
            "identifierType": "'${3}'",
            "exchangeCode": "",
            "pricingSource": ""
        }
    ]
}' | jq

curl --location --request POST http://vm-act-med-prd-app-01:9020/select/api/submissions/${h1} \
--header 'Content-Type: application/json' \
--data '{
   "id":'${1}',
   "status":"SUBMITTED"
}' | jq
}


## submit for each entry in file
id=${1}
file=${2}
defaultType=${3}
for entry in `cat ${file}`
do
        identifier=`echo $entry | cut -d"|" -f1`
        [[ -z "`echo $entry | cut -d"|" -f2`" ]] && identifierType=${defaultType} || identifierType=`echo $entry | cut -d"|" -f2`
        submit $id $identifier $identifierType
done

