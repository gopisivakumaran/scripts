#!/bin/bash

unalias -a
set -o pipefail
. ac_utilities.sh
dt=`date +'%Y%m%d_%H:%M:%S'`
mkdir $AC_WORKDIR/ac-select/json/${dt}

# variables
server="vm-act-med-prd-app-01"
port=9020
cache_period=900000 #in milliseconds

usage(){
echo -e "usage:\n${0}
  -t <path to template file>
  -p <programme name, e.g. getData>
  -r <range for getHistory>
  -f <programme flag, e.g. adhoc>
  -c <path to file containing list of download categories>
  -i <path to file containing list of identifier|identifierType>
  -n <request name>
";
}

if [ $# -eq 0 ]
then usage;
exit 0; fi

while getopts "t:p:r:f:c:i:n:" opt; do
  case ${opt} in
    t ) template=${OPTARG}
      ;;
    p ) progname=${OPTARG}
      ;;
    r ) historyRange=${OPTARG}
      ;;
    f ) progflag=${OPTARG}
      ;;
    c ) categories=${OPTARG}
      ;;
    i ) identifiers=${OPTARG}
      ;;
    n ) name=${OPTARG}
      ;;	 
    \? ) usage
      ;;
  esac
done
shift $((${OPTIND} -1))

nm=${name}.${dt}
## retrieving request ID
ac_log_info "Retrieving request ID."
reqId=`curl 2>/dev/null --location --request POST http://${server}:${port}/select/api/requests \
--header 'Content-Type: application/json' \
--data '{
   "name":"'${nm}'",
   "dataSource":{
      "id":"BB"
   }
}' | jq '.id'`

check_rc "request name has already been used" 2

## setting template
ac_log_info "Setting template for request"
tmp=template.json
[[ -f ${tmp} ]] && rm ${tmp}
cat << EOF >> ${tmp}
{
   "id": ${reqId},
   "attributes":[
EOF

for line in `cat ${template}`
do
echo "{" >> ${tmp}
echo '"id":"'${line}'"' >> ${tmp}
echo "}," >> ${tmp}
done
cat << EOF >> ${tmp}
  ]
}
EOF
count=$(cat ${tmp} | wc -l)
sed -i $((${count}-2))s/.$// ${tmp}

cp ${tmp} $AC_WORKDIR/ac-select/json/${dt}
curl --location --request PATCH http://${server}:${port}/select/api/requests/${reqId} \
--header 'Content-Type: application/json' \
--data @${tmp} | jq

check_rc "invalid template" 2
rm ${tmp}

## setting properties
ac_log_info "Setting properties for request"
tmp=properties.json
[[ -f ${tmp} ]] && rm ${tmp}
cat << EOF >> ${tmp}
{
"id":${reqId},
   "propertyValues":[
      {
         "id":1,
         "value":"${progname}",
         "valueId":1,
         "name":"PROGRAMNAME",
         "type":"single"
      },
EOF

if [ ${progname} == "getHistory" ]
then
cat << EOF >> ${tmp}
{
"id":6,
"value":"`echo ${historyRange} | cut -d'|' -f1`",
"valueId":null,
"name":"startDate",
"type":"single"
},
{
"id":7,
"value":"`echo ${historyRange} | cut -d'|' -f2`",
"valueId":null,
"name":"endDate",
"type":"single"
},
EOF
fi

for line in `cat ${categories}`
do
cat << EOF >> ${tmp}
{
      "id":2,
      "value":"${line}",
      "valueId":3,
      "name":"downloadCategories",
      "type":"array"
},
EOF
done


cat << EOF >> ${tmp}
{
      "id":5,
      "value":"${progflag}",
      "valueId":27,
      "name":"PROGRAMFLAG",
      "type":"single"
},
{
      "id":30,
      "value":${cache_period},
      "valueId":null,
      "name":"ELAPSEDTIME",
      "type":"single"
}
]
}
EOF

cp ${tmp} $AC_WORKDIR/ac-select/json/${dt}
curl 2>/dev/null --location --request PATCH http://${server}:${port}/select/api/requests/${reqId} \
--header 'Content-Type: application/json' \
--data @${tmp} | jq

check_rc "invalid properties" 2
rm ${tmp}

## setting instruments
ac_log_info "Setting identifiers for request"
tmp=instruments.json
[[ -f ${tmp} ]] && rm ${tmp}
cat <<EOF >> ${tmp}
{
    "datasource": "BLOOMBERG",
    "request": "${nm}",
    "instruments": [
EOF

#for line in `cat ${identifiers}`
cat ${identifiers} | while read line;
do
if [ -n "`echo $line | grep "|"`" ]
then
        identifier=`echo $line | cut -d"|" -f1`
        identifierType=`echo $line | cut -d"|" -f2`
else
        identifier=$line
        identifierType=BB_GLOBAL
fi
cat << EOF >> ${tmp}
        {
            "instrumentType": "$identifierType",
            "instrumentId": "$identifier",
                     "exchange": {
                "code": ""
            },
                     "pricingSource": {
                "code": ""
            }
        },
EOF
done
cat << EOF >> ${tmp}
        ]
}
EOF
count=$(cat ${tmp} | wc -l)
sed -i $((${count}-2))s/.$// ${tmp}

cp ${tmp} $AC_WORKDIR/ac-select/json/${dt}

echo ${name} > /u01/chroot/home/acsftp/processing/ID_$(($reqId + 1)).acselect

ac_log_info "submitting full request"
## submitting full request & handling response
resp=`curl 2>/dev/null --location --request POST http://${server}:${port}/select/api/public/v1/request/execute \
--header 'Content-Type: application/json' \
--data @${tmp} | jq '.instruments[].requested'`

check_rc "invalid identifiers" 2
rm ${tmp}

if [ -n "`echo ${resp} | grep true`" ]
then
	exit 0;
elif [ -n "`echo ${resp} | grep false`" ] && [ -z "`echo ${resp} | grep true`" ]
then
	exit 1;
else
	exit 2;
fi

