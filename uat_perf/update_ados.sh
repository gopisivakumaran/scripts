#!/usr/bin/bash

prefix=$1
ado_num=$2
attr_num=$3
upd_num=$4

[[ -f update.txt ]] && rm update.txt

for ((i=1; i<=${ado_num}; i++))
do
        echo "TRANS" >> update.txt
        echo 'OBJECT ADO ADD symbol="'${prefix}.${i}'"' >> update.txt
	echo 'longname="'${prefix}.${i}'" template="'${prefix}'"' >> update.txt

        for ((j=1; j<=${attr_num}; j++))
        do

                for ((k=1; k<=${upd_num}; k++))
                do
                echo 'OBJECT ADO_ATTRIBUTE ADD symbol="'${prefix}.${i}'"' >> update.txt
		echo 'attrid="'${prefix}.${j}'"' >> update.txt
		echo 'value="'`pwgen -s -1 10`'" status=1' >> update.txt
                done

        done

done

ac_bl update.txt && rm update.txt
