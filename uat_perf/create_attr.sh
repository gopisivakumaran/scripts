#!/usr/bin/bash

prefix=$1
attr_num=$2

[[ -f attrs.txt ]] && rm attrs.txt

echo -e 'TRANS\n' >> attrs.txt

for ((i=1; i<=${attr_num}; i++))
do
	echo 'OBJECT STATICATTRIBUTE ADD attrid="'${prefix}.${i}'"' >> attrs.txt
	echo 'longname="'${prefix}.${i}'" derived="0" datatp="3"' >> attrs.txt
	echo 'multivalued="0" optional="0" profiling="0" unique="0"' >> attrs.txt
done

ac_bl attrs.txt && rm attrs.txt

[[ -f attrs.txt ]] && rm attrs.txt
echo -e 'TRANS\n' >> tmpl.txt
echo -e 'TRANS\nOBJECT LIST INSERT listid="'${prefix}'" longname="'${prefix}'" listtp="2"' | ac_bl -
for ((i=1; i<=${attr_num}; i++))
do
	echo 'OBJECT LIST_CONTENTS INSERT listid="'${prefix}'"' >> tmpl.txt
	echo 'longname="21052020" entry="'${prefix}.${i}'"' >> tmpl.txt
done

ac_bl tmpl.txt && rm tmpl.txt

