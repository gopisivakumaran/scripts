#!/usr/bin/bash

prefix=$1
ado_num=$2
attr_num=$3

[[ -f trans.txt ]] && rm trans.txt

for (( i = 1; i <= ${ado_num}; i++ ))
do
	echo "TRANS" >> trans.txt
	echo 'OBJECT ADO ADD symbol="'${prefix}.${i}'"' >> trans.txt
	echo 'longname="'${prefix}.${i}'" template="'${prefix}'"' >> trans.txt
	echo 'OBJECT ADO_ATTRIBUTE ADD symbol="'${prefix}.${i}'"' >> trans.txt
	echo 'attrid="acx-status" value=1 status=1' >> trans.txt
for (( j = 1; j <= ${attr_num}; j++ ))
do
	echo 'OBJECT ADO_ATTRIBUTE ADD symbol="'${prefix}.${i}'"' >> trans.txt
	echo 'attrid="'${prefix}.${j}'" value="'`pwgen -s -1 10`'" status=1' >> trans.txt
done
done

ac_bl trans.txt && rm trans.txt

