Usage

./main.sh <number of ados> <number of attributes per ado> <updates per attributes>

You can run main.sh script, and set following arguments
1. the number of ados you want to create
2. the number of attributes each ado will have
3. the number of updates per attribute

e.g. ./main.sh 100000 100 100

will send 100000 ados with 100 attributes each. Each attribute will be updated 100 times.
