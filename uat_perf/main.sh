#!/usr/bin/bash

usage(){ echo "${0} <number of ados> <number of attributes> <number of updates per attribute>"; }

if [ $# -eq 0 ]
then usage; 
exit 0; fi

prefix=C0.`date +%b%d`.`uuidgen | cut -d- -f2`
ado_num=$1
attr_num=$2
upd_num=$3

echo "INFO: ados and attributes will have prefix ${prefix}"
./create_attr.sh ${prefix} ${attr_num}
echo "INFO: waiting for metadata to consume into acx before creating txn data"
sleep 60
./create_ados.sh ${prefix} ${ado_num} ${attr_num}
./update_ados.sh ${prefix} ${ado_num} ${attr_num} ${upd_num}
