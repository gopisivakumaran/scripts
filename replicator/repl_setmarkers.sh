#!/bin/bash

##-----------------------------------------------------------------------------
# Name:
#   repl_setmarkers.sh -- Set transaction markers.
#
# Usage:
#   repl_setmarkers.sh <ptlm> [ -u destination ]
#
#   For Example, 
#   Analyze mode: repl_setmarkers.sh 1,4,7823336
#   ptlms.csv is generated for all nodes
#
#   Update mode: repl_setmarkers.sh 1,4,7823336 -u 536877534@localhost 
#   replication_markers table will be updated with ptlms from ptlms.csv 
#   for specified destination and memory mapped files are deleted.  
#
# Options:
#   -h           show this help
#   -u           update the private transaction markers in AC Server
#                must supply "<rpc_address>@server" as argument.
#
# Description:
#   This script analyses the transactions in AC Server transaction log,
#   from a given ptlm.
#   In update mode, it will set the transaction markers in replication_markers table
#   and deletes the .rtn, .rtp and .ptlm files.
#
# Output:
#   ptlms.csv (created in current working directory)
##-----------------------------------------------------------------------------

. ac_utilities.sh

help() {
    sed -n '/^##--/,/^##--/{s/^#$//p;s/^#.-*//p;}' "$0"
}

[[ $# -eq 0 ]] && help && exit

while getopts ":h" opt
do
    case "$opt" in
    h)  help
        exit 0
        ;;
    *)  echo "A ptlm must be specified" >&2
        exit 2
        ;;
    esac
done
shift $((OPTIND -1))

ptlm="$1"
shift
doupdate=false
while getopts "u:" opt
do
    case "$opt" in
    u)  target=${OPTARG}
	doupdate=true
        ;;
    *)  echo "$prgname: unknown option -$OPTARG" >&2
        echo "Use -h for help." >&2
        exit 2
        ;;
    esac
done
shift $((OPTIND - 1))


## security checks
clusterMap=`echo "select count(*) from cluster_map where server_tp = 1" | ac_bl -qs -`
updateServers=`adstart2 -v | grep -c ac_updserv_server`

ac_log_info "No. update server entries in cluster map = $clusterMap"
ac_log_info "No. update server instances = $updateServers"
if [[ $clusterMap != $updateServers ]]
then
ac_log_error "cluster map does not match number of update server instances, exiting.."
exit 1
else
ac_log_info "cluster map matches update server instances, security check passed"
fi

nodeDir=`for dir in $AC_LOGDIR/node_00*; do echo ${dir: -1}; done`
nodeNum=`echo "select node_num from cluster_map where server_tp=1 order by 1" | ac_bl -qs -`

if [[ $nodeDir != $nodeNum ]]
then 
ac_log_error "node directory names do not match entries in cluster map, exiting.."
exit 1
else
ac_log_info "node directory names match entries in cluster map, security check passed"
fi

## main
#ptlm="$1"
us_id=${US_ID:-1}
find_markers(){
node=`echo $ptlm | awk -F, '{print $1}'`
file=`echo $ptlm | awk -F, '{print $2}'`
offset=`echo $ptlm | awk -F, '{print $3}'`

txn_id=`ac_pt -s -n $node -l $file $offset 1 | grep '^Transaction ID' | awk '{print $NF}'`
txn=`echo $txn_id | awk -F: '{print $2}' | awk -F. '{print $1}'`
txt=ptlms.csv
echo "Node,Backup_No,Offset,Transaction_ID" > $txt

for k in `ls $AC_LOGDIR | grep node | grep -o .$`
do
	[[ k -eq `echo $ptlm | awk -F, '{print $1}'` ]] && echo "$ptlm,$txn_id" >> $txt && continue 
	found=false
	txn_adj=0
	jnl_num=`ls $AC_LOGDIR/node_00${k} | grep \\.jnl$ | wc -l`
	for((i=0; i<${jnl_num}; i++))
	do
	lst=`ac_pt -s -n $k -l $i 1 999999999 | grep '^Transaction ID' | awk '{print $NF}' | awk -F: '{print $2}' | awk -F. '{print $1}'`
		for j in $lst
		do
			if [[ $j -gt $txn ]]
			then
                		found=true
				txn_adj=$j
				break;
			fi
		done
	[[ $found == true ]] && break;
	done
	bytes=`ac_pt -s -n $k -l $i 1 999999999 | egrep '^Transaction ID|Transaction Offset' | grep "Transaction ID: $us_id:$txn_adj.0" -B1 | head -1 | awk '{print $NF}'`
	echo "${k},${i},${bytes},$us_id:${txn_adj}.0" >> $txt
done

ac_log_info "transaction at given ptlm is $txn_id"
ac_log_info "ptlms for other nodes are written to ptlms.csv"
ac_log_info "run with -u option to set transaction markers"

}


update_markers(){
[[ ! -f ptlms.csv ]] && echo "ptlms.csv not found. Run script in analyze mode first" && exit 1
ac_log_info "updating replication_markers table with input ptlm and ptlms generated in ptlms.csv"

for n in `ls $AC_LOGDIR | grep node | grep -o .$`
do
line=`cat ptlms.csv | awk -F, '{print $1}' | grep -n $n | awk -F: '{print $1}'`
b=`sed -n ${line}p ptlms.csv | awk -F, '{print $2}'`
o=`sed -n ${line}p ptlms.csv | awk -F, '{print $3}'`
t1=`echo $target | awk -F@ '{print $1}'`
t2=`echo $target | awk -F@ '{print $2}'`
echo -e "TRANS\nSQL update replication_markers set server='$t2', rpc_address='$t1', backup_no='$b', logfile_offset='$o' where node_num=$n" | ac_bl -
done

ac_log_info "removing .rtn, .rtp, .ptlm files from $AC_LOGDIR"
rm $AC_LOGDIR/*.rtn $AC_LOGDIR/*.rtp $AC_LOGDIR/*.ptlm
}

if $doupdate
then
	update_markers
else
	find_markers
fi
