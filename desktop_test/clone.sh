#!/bin/bash

usage(){
	echo "usage: ./clone.sh <ado-id> <number of clones>"
}

if [ $# -eq 0 ]
then
	usage
	exit 0
fi

ado=$1
num=$2

[[ -f static.txt ]] && rm static.txt
[[ -f timeseries.txt ]] && rm timeseries.txt

echo "${ado}" | ac_evalform -a " return dumpcurrent();" -f $AC_SYSTEM/formulas/dumpado.fe -l - > static.txt
echo "${ado}" | dumpdata > timeseries.txt

for((i=1; i<=${num}; i++))
do
	if [ ${i} -eq 1 ]
	then
		sed -i "s/${ado}/&.${i}/g" static.txt
		cat static.txt | ac_bl -
		sed -i "s/${ado}/&.${i}/g" timeseries.txt
                cat timeseries.txt | amd_pr -AefzZ -
	else
		sed -i "s/${ado}.$((${i} - 1))/${ado}.${i}/g" static.txt
		cat static.txt | ac_bl -
		sed -i "s/${ado}.$((${i} - 1))/${ado}.${i}/g" timeseries.txt
                cat timeseries.txt | amd_pr -AefzZ -
	fi
done
rm static.txt timeseries.txt
