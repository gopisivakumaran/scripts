#!/bin/bash

curl --location --request POST 'http://ac-demo-1.acx-sandbox.net:9020/select/api/public/v1/request/execute' \
--header 'Content-Type: application/json' \
--data-raw '{
    "datasource": "BLOOMBERG",
    "request": "'$1'",
    "instruments": [
        {
            "instrumentType": "ISIN",
            "instrumentId": "NL0000313286",
                     "exchange": {
                "code": ""
            },
                     "pricingSource": {
                "code": ""
            }
        },
        {
            "instrumentType": "ISIN",
            "instrumentId": "NL0000018034",
             "exchange": {
                "code": ""
            },
                     "pricingSource": {
                "code": ""
            }
        }
        ],
    "settings": {
        "expectedDuration": {
            "time": 10,
            "unit": "MINUTES"
        }
    }
}'
